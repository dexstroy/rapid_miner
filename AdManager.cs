﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdManager : MonoBehaviour {

    private BannerView bannerView;
    private AdRequest request;

    public void Start() {
        this.RequestBanner();
    }

    public void RequestBanner() {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-8367711333838967/4642372039";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
            string adUnitId = "unexpected_platform";
#endif
        
        // naredi banner view
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);

        // Create an empty ad request.
        request = new AdRequest.Builder().Build();
        // nalozi reklamo
        bannerView.LoadAd(request);
        // skrije reklamo
        bannerView.Hide();

    }

    public void showBanner() {
        // prikaze banner
        bannerView.Show();
    }


    public void hideBanner() {
        bannerView.Hide ();
        bannerView.Destroy ();
        RequestBanner();

    }
}
