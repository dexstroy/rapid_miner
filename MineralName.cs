﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MineralName : MonoBehaviour {

    float startY;
    public Color32 barva = new Color32(255,255,255,255);
    private void Start() {
        startY = transform.position.y;
    }

    // Update is called once per frame
    void Update () {
        // pomika ga navzgor
        transform.Translate(0, 0.5f * Time.deltaTime, 0);

        // spreminja prosojnost
        //gameObject.GetComponentInChildren<Text>().color = new Color32(255,255,255, (byte)( (1 - Mathf.Abs(startY - transform.position.y)) * 255));
        gameObject.GetComponentInChildren<Text>().color = new Color32(barva.r, barva.g, barva.b, (byte)((1 - Mathf.Abs(startY - transform.position.y)) * 255));

        // ko se zadosti oddalji ga izbrise
        if (Mathf.Abs(startY - transform.position.y) >= 1) {
            Destroy(gameObject);
        } 
	}

    // nastavi text otroku tega canvasa
    public void setText(string text) {
        gameObject.GetComponentInChildren<Text>().text = text;
    }
}
