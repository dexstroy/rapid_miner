﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class RewardedAd : MonoBehaviour {
    public GameObject miner;
    public RewardBasedVideoAd rewardBasedVideo;
    public bool reward = false;

    // v primeru da se reklama ne nalozi jo poizkusa naloziti v 10s intervalih
    public bool failedToLoad = false;

    private void Start() {

    #if UNITY_ANDROID
            string appId = "ca-app-pub-8367711333838967~4287148815";
    #elif UNITY_IPHONE
            string appId = "ca-app-pub-3940256099942544~1458002511";
    #else
            string appId = "unexpected_platform";
    #endif

        //MobileAds.SetiOSAppPauseOnBackground(true);
        

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;

        // RewardBasedVideoAd is a singleton, so handlers should only be registered once.
        this.rewardBasedVideo.OnAdLoaded += this.HandleRewardBasedVideoLoaded;
        this.rewardBasedVideo.OnAdFailedToLoad += this.HandleRewardBasedVideoFailedToLoad;
        this.rewardBasedVideo.OnAdOpening += this.HandleRewardBasedVideoOpened;
        this.rewardBasedVideo.OnAdStarted += this.HandleRewardBasedVideoStarted;
        this.rewardBasedVideo.OnAdRewarded += this.HandleRewardBasedVideoRewarded;
        this.rewardBasedVideo.OnAdClosed += this.HandleRewardBasedVideoClosed;
        this.rewardBasedVideo.OnAdLeavingApplication += this.HandleRewardBasedVideoLeftApplication;

        RequestRewardBasedVideo();

        InvokeRepeating("slowUpdate", 2, 10);
    }


    private void Update() {
        if (reward) {
            // vrne elemente minerju
            miner.GetComponent<Kopac>().returnElements();
            // naredi ponovno zahtevo za nalozitev novega posneka
            RequestRewardBasedVideo();

            reward = false;
        }
    }

    // za ponovne zahteve v primeru izpada interneta
    private void slowUpdate() {
        if (failedToLoad) {
            print("oddal ponovni zahtevek za reklamo");
            // naredi ponovno zahtevo
            RequestRewardBasedVideo();
            failedToLoad = false;
        }
    }

    public void RequestRewardBasedVideo() {
        #if UNITY_EDITOR
                string adUnitId = "unused";
        #elif UNITY_ANDROID
                string adUnitId = "ca-app-pub-8367711333838967~4287148815";
                print("Izbral Android");
        #elif UNITY_IPHONE
                string adUnitId = "ca-app-pub-3940256099942544/1712485313";
        #else
                string adUnitId = "unexpected_platform";
        #endif

        this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), adUnitId);
    }

    public void ShowRewardBasedVideo() {
        if (this.rewardBasedVideo.IsLoaded()) {
            this.rewardBasedVideo.Show();
        }
        else {
            MonoBehaviour.print("Reward based video ad is not ready yet");
        }
    }


    // Returns an ad request with custom ad targeting.
    private AdRequest CreateAdRequest() {
        return new AdRequest.Builder()
            .SetBirthday(new DateTime(1985, 1, 1))
            .AddExtra("color_bg", "9B30FF")
            .Build();
    }


    #region RewardBasedVideo callback handlers

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args) {
        print("EVENT: reklama uspesno nalozena");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
        print("EVENT: failed to load: " + args.ToString());
        
        failedToLoad = true;
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args) {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args) {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args) {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args) {
        string type = args.Type;
        double amount = args.Amount;
        print(
            "HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);

        // imam spremenjlivko za reward, saj se to izvede v niti in nemorem opravljati klicev izven funkcije
        reward = true;
        print("Spremenil reward boolean");
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args) {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    #endregion

}
