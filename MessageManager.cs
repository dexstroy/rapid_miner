﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour {
    public GameObject messageBox;
    public GameObject addMessage;
    public GameObject miner;
    public GameObject adManager;

    // ima kazalce kjer za ima zracunano kdaj je opozorilo
    public GameObject ratioObj;

    private string startMessage = "Welcome to Rapid miner!\nYour goal is to dig as many minerals you can and finish the boss!\nHave fun!";


    private string lowFuelMessage = "Your fuel tank is very low.\nOn surface visit gas station a soon as possible!";
    private string temperatureMessage = "Temperature is too high for your current radiator.\nGo to the surface and buy better radiator in store.";
    private string lowMineralSpace = "Your mineral storage is full. On surface go to the mineral market and sell your minerals.\n" +
                                     "If you want to carry more minerals buy a bigger mineral storage";

    private string fuelStoreMessage = "This is a gas station.\n" +
                                      "Drag a slider to specify the amount of fuel you want to buy.";
    private string mineralMarketMessage = "This is a mineral market.\nHere you can sell your minerals.";
    private string storeMessage = "This is a store where you can buy better parts for your miner.";
    private string repairShopMessage = "This is a repair shop, where you can fix any damage on the miner.\nDrag a slider to specify how much you want to fix it.";


    private void Start() {
        InvokeRepeating("secondUpdate", 2, 1);
    }


    // za prikaz sporocil, da se ne ponovijo veckrat
    /*Ima atribute ki si sledijo po vrsti
     * 0 Ko prvic prides v igro
     * 1 Ko ti prvic zmanjka bencina
     * 2 Ko se ti prvic dvigne temperatura
     * 3 Ko prvic obisces fuelPump
     * 4 Ko prvic obisces mineralStore
     * 5 Ko prvic obisces Store
     * 6 Ko prvic obisces Repair shop
     * 7 Ko ti prvic zmanjka prostora v zbiralniku
     */
    // metoda ki jo klicejo stavbe ob prvem vstopu da se prikaze sporocilo
    public void buildingMessage(int i) {
        // fuelPump
        if (i == 3) {
            if (miner.GetComponent<Xml>().messages[i] == 0) {
                // pokaze sporocilo
                miner.GetComponent<Kopac>().pauseGame(true);
                GameObject message = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
                message.GetComponent<MessageContairner>().setText(fuelStoreMessage);
                // zapise da je bilo sporocilo predvajano
                miner.GetComponent<Xml>().messages[i] = 1;
            }
        }

        // mineralMarket
        else if (i == 4) {
            if (miner.GetComponent<Xml>().messages[i] == 0) {
                // pokaze sporocilo
                miner.GetComponent<Kopac>().pauseGame(true);
                GameObject message = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
                message.GetComponent<MessageContairner>().setText(mineralMarketMessage);
                // zapise da je bilo sporocilo predvajano
                miner.GetComponent<Xml>().messages[i] = 1;
            }
        }

        // store
        else if (i == 5) {
            if (miner.GetComponent<Xml>().messages[i] == 0) {
                // pokaze sporocilo
                miner.GetComponent<Kopac>().pauseGame(true);
                GameObject message = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
                message.GetComponent<MessageContairner>().setText(storeMessage);
                // zapise da je bilo sporocilo predvajano
                miner.GetComponent<Xml>().messages[i] = 1;
            }
        }

        // repairShop
        else if (i == 6) {
            if (miner.GetComponent<Xml>().messages[i] == 0) {
                // pokaze sporocilo
                miner.GetComponent<Kopac>().pauseGame(true);
                GameObject message = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
                message.GetComponent<MessageContairner>().setText(repairShopMessage);
                // zapise da je bilo sporocilo predvajano
                miner.GetComponent<Xml>().messages[i] = 1;
            }
        }

        
    }

    // vsako sekundo preveri ali mora poslati kako sporocilo
    public void secondUpdate() {
        // preveri ali je zacetek igre
        if (miner.GetComponent<Xml>().messages[0] == 0) {
            miner.GetComponent<Kopac>().pauseGame(true);
            GameObject message = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
            message.GetComponent<MessageContairner>().setText(startMessage);
            // zapise da je bilo sporocilo predvajano
            miner.GetComponent<Xml>().messages[0] = 1;
        }

        // preveri stanje goriva
        if (miner.GetComponent<Xml>().messages[1] == 0) {
            // preveri ali ga ima premalo
            if (ratioObj.GetComponent<Ratio>().fuelBlink) {
                miner.GetComponent<Kopac>().pauseGame(true);
                GameObject message = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
                message.GetComponent<MessageContairner>().setText(lowFuelMessage);
                // zapise da je bilo sporocilo predvajano
                miner.GetComponent<Xml>().messages[1] = 1;
            }
        }

        // preveri temperaturo
        if (miner.GetComponent<Xml>().messages[2] == 0) {
            // preveri ali je temperatura prevelika
            if (ratioObj.GetComponent<Ratio>().hot) {
                miner.GetComponent<Kopac>().pauseGame(true);
                GameObject message = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
                message.GetComponent<MessageContairner>().setText(temperatureMessage);
                // zapise da je bilo sporocilo predvajano
                miner.GetComponent<Xml>().messages[2] = 1;
            }
        }

        // zmanjka prostora v zbiralniku
        
        
            if (miner.GetComponent<Xml>().messages[7] == 0) {
            // preveri ali mu le zmanjkalo prostora
            if (miner.GetComponent<Xml>().storageCapacity[miner.GetComponent<Xml>().selectedStorage] <= miner.GetComponent<Xml>().shramba_elementi()) {
                miner.GetComponent<Kopac>().pauseGame(true);
                GameObject message = Instantiate(messageBox, new Vector3(0, 0, 0), Quaternion.identity);
                message.GetComponent<MessageContairner>().setText(lowMineralSpace);
                // zapise da je bilo sporocilo predvajano
                miner.GetComponent<Xml>().messages[7] = 1;
                }
                
            }
            
        }

    // ob klicu se naredi zahteva za rewarded add
    public void rewardedAdd() {
        miner.GetComponent<Kopac>().pauseGame(true);
        GameObject message = Instantiate(addMessage, new Vector3(0, 0, 0), Quaternion.identity);
        message.GetComponent<AddMessage>().setText("You died and lost all your minerals \n You can watch ad and get all you minerals back");
    }

    
    
}
