﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Ratio : MonoBehaviour {
    public GameObject miner;

    public GameObject fuel_merilec;
    public GameObject fuel_kazalec;

    public GameObject temp_merilec;
    public GameObject temp_kazalec;

    public GameObject containerSlider;
    public GameObject healthSlider;

    // ko je v vrocem obmocju je true
    public bool hot = false;

    public bool fuelBlink = false;
    
    // true -rdeca, false zelena, za utripanje barv
    private bool tempKazalecbarva = false;

    // true -rdeca, false zelena, za utripanje barv
    private bool fuelKazalecbarva = false;

    public Text dynamiteNumber;
    public Text electricNumber;
    public Text fuelNumber;
    public Text teleporterNumber;
    public Text healthhNumber;

    
    public Text containerText;




    // Use this for initialization
    void Start () {
        // sirina kazalca in dolzina za polovico sirine merilca
        fuel_kazalec.GetComponent<RectTransform>().sizeDelta = new Vector2(fuel_merilec.GetComponent<RectTransform>().rect.width / 15, fuel_merilec.GetComponent<RectTransform>().rect.width / 2);
        // anchor pozicija kazalca
        fuel_kazalec.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, fuel_merilec.GetComponent<RectTransform>().rect.width / 2);

        // sirina kazalca in dolzina za polovico sirine merilca
        temp_kazalec.GetComponent<RectTransform>().sizeDelta = new Vector2(temp_merilec.GetComponent<RectTransform>().rect.width / 15, temp_merilec.GetComponent<RectTransform>().rect.width / 2);
        // anchor pozicija kazalca
        temp_kazalec.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, temp_merilec.GetComponent<RectTransform>().rect.width / 2);
        //print("Size y: " + fuel_kazalec.GetComponent<RectTransform>().anchoredPosition.y);

        //temp_kazalec.GetComponent<RectTransform>().rotation = Quaternion.Euler(0.0f, 0f, 63);
       

        
        
        InvokeRepeating("updateValues", 0, 0.1f);
        InvokeRepeating("hotZone", 0, 2);
    }


    public void updateFuelGauge() {

        // izracuna procente glede na tip tanka in kolicino goriva
        
        float procenti = (miner.GetComponent<Xml>().fuelState * 100) / miner.GetComponent<Xml>().fuelCapacity[miner.GetComponent<Xml>().selectedFuel];

        // izracuna za koliko stopinj se mora pomakniti v desno od -62
        float kot = (124 * procenti) / 100;
        fuel_kazalec.GetComponent<RectTransform>().rotation = Quaternion.Euler(0.0f, 0f, -62 + kot);
    }


    public void updateTempGauge() {
        // izracuna procente glede na tip tanka in kolicino goriva

        float procenti = 100 - (-miner.transform.position.y * 10 * 100) / miner.GetComponent<Xml>().radiatorDepth[miner.GetComponent<Xml>().selectedRadiator];
        
        // omeji kazalec na obmocje
        if (procenti < 1) {
            procenti = 1;
            hot = true;
        }
        else if (procenti > 100) {
            procenti = 100;
            hot = false;
        }
        else {
            hot = false;
        }

        // izracuna za koliko stopinj se mora pomakniti v desno od -62
        float kot = (124 * procenti) / 100;
        temp_kazalec.GetComponent<RectTransform>().rotation = Quaternion.Euler(0.0f, 0f, -62 + kot);
    }

    public void updateHealthSlider() {
        float procenti = miner.GetComponent<Xml>().health / miner.GetComponent<Xml>().hullHealth[miner.GetComponent<Xml>().selectedHull];
        healthSlider.GetComponent<RectTransform>().anchorMax = new Vector2(procenti, 1);
        
        
    }

    public void updateContainerSlider() {

        float sliderPosition =  miner.GetComponent<Xml>().shramba_elementi() / (float)miner.GetComponent<Xml>().storageCapacity[miner.GetComponent<Xml>().selectedStorage];
        if (sliderPosition + 0.03f > 1) sliderPosition = 1 - 0.03f;
        containerSlider.GetComponent<RectTransform>().anchorMax = new Vector2(sliderPosition + 0.03f, 1);

        containerText.text = miner.GetComponent<Xml>().shramba_elementi() + "/" + miner.GetComponent<Xml>().storageCapacity[miner.GetComponent<Xml>().selectedStorage];
    }

    // klicana 10x na sekundo
    public void updateValues() {
        if (!miner.GetComponent<Xml>().loaded) return;
        updateFuelGauge();
        updateTempGauge();
        updateContainerSlider();
        updateHealthSlider();


        // animacija utripanja kazalca
        if (hot) {
            // za uripanje
            if (tempKazalecbarva) {
            tempKazalecbarva = false;
            temp_kazalec.GetComponent<Image>().color = new Color32(255, 0, 0, 255);
        }
        // 
        else {
            tempKazalecbarva = true;
            temp_kazalec.GetComponent<Image>().color = new Color32(0, 255, 0, 255);
        }
    }
        // drugace normalno obarva kazalec za temperaturo
        else {
            temp_kazalec.GetComponent<Image>().color = new Color32(0, 255, 0, 255);
        }

        // preveri ali ima kriticno goriva
        if ((miner.GetComponent<Xml>().fuelState / miner.GetComponent<Xml>().fuelCapacity[miner.GetComponent<Xml>().selectedFuel]) <= 0.1) {
            fuelBlink = true;
        }
        else {
            fuelBlink = false;
        }

        // animacija za malo goriva
        if (fuelBlink) {
            if (fuelKazalecbarva) {
                fuel_kazalec.GetComponent<Image>().color = new Color32(255, 0, 0, 255);
                fuelKazalecbarva = false;
            }
            else {
                fuel_kazalec.GetComponent<Image>().color = new Color32(0, 255, 0, 255);
                fuelKazalecbarva = true;
            }
        }
        else {
            fuel_kazalec.GetComponent<Image>().color = new Color32(0, 255, 0, 255);
        }
    }

    // prozi se vsako sekundo in preveri ali je miner v kriticnem polozaju
    public void hotZone() {
        // ko je v vrocem obmocju
        if (hot && !miner.GetComponent<Kopac>().pause) {
            // klice animacijo rdec zaslon
            miner.GetComponent<Kopac>().damageAnimation();
            // odsteje zdravje
            miner.GetComponent<Xml>().health -= 1;
        }   
    }

    // aktivira jo gumb za nazaj v menu
    public void backToMenu() {
        miner.GetComponent<Xml>().saveXML();
        //Application.LoadLevel("Menu");
        SceneManager.LoadScene("Menu");
    }


    // za stevilo special elementov
    /*
     * 0 Dynamite
     * 1 Electric bomb
     * 2 fuel
     * 3 teleporter
     */
    // nastavi stevilo elemetov v gumbih
    public void updateSpecialElementNumber() {

        dynamiteNumber.text = miner.GetComponent<Xml>().specialElements[0].ToString();
        electricNumber.text = miner.GetComponent<Xml>().specialElements[1].ToString();
        fuelNumber.text = miner.GetComponent<Xml>().specialElements[2].ToString();
        teleporterNumber.text = miner.GetComponent<Xml>().specialElements[3].ToString();
        healthhNumber.text = miner.GetComponent<Xml>().specialElements[4].ToString();
    }
}
