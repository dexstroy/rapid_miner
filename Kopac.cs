﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;



// za upravljanje kopaca
public class Kopac : MonoBehaviour {
    public Rigidbody2D kopac;
    public GameObject boss;



    // kamera za sledenje minerju
    public Camera kamera;

    // za premikanje kopacas
    public float flySpeed;
    public float speed;
    public float trenje;
    public float kopanjeSpeed;
    public float maxKopanje = 0;

    // raycasti za zaznavanje objektov okoli sebe
    private RaycastHit2D left;
    private RaycastHit2D right;
    private RaycastHit2D bottom;

    // referenca do animatorja
    public Animator animator;

    // stanja kopaca
    private bool kopanje = false;
    public  bool premikanje = true;

    public bool pause;

    /*
    0 - bottom
    1 - left
    2 - right
    */
    private int kopanjeSmer = 0;

    // float ki pove do kje je ze skopal
    private float izkopano = 0;

    // mineral, ki ga trenutno kopljemo
    private GameObject kopani;

    

    public ParticleSystem sideDust;
    public ParticleSystem bottomDust;
    public ParticleSystem izpuh;

    Vector2 moveVector;



    public bool dead = false;

    // efekti
    public GameObject mineralText;
    public GameObject dinamit;
    public GameObject teleportEffect;
    public GameObject electricBomb;
    public GameObject destroyColorScreen;
    public GameObject destroyAnimation;
    public GameObject body;





    // zvoki
    public AudioSource bombSound;
    public AudioSource ebombSound;
    public AudioSource destroySound;
    public AudioSource softBeep; // ko se v zbiralnik doda element
    public AudioSource fullBeep; // ko zbiralnik poln
    public AudioSource helicopterSound;
    public AudioSource motorIdle;
    public AudioSource addFuelSound;
    public AudioSource hitSound;
    public AudioSource blagajna;

    // za dostopane do ratio skripte
    public GameObject stanjeCanvas;


    // za prikaz sporocila rewarded add
    //public GameObject objController;
    public GameObject adManager;
    
    // shrani izgubljene elemente ko umres
    public Dictionary<int, int> shrambaRewardAdd;
    public bool canPlayAdd = false;



    // Run only once 
    private void Start()
    {
        // nastavim animacije v pravo stanje
        sideDust.Stop();
        bottomDust.Stop();
        sideDust.gameObject.SetActive(false);
        bottomDust.gameObject.SetActive(false);

        
        

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        // postavi minerja na pozicijo
        transform.position = new Vector2(19, 1);
    }


    // Update is called once per frame
    void Update () {


        if (!motorIdle.isPlaying) motorIdle.Play();
        if (!premikanje) motorIdle.pitch = 0;
        else if (kopanje) motorIdle.pitch = 1.7f;
        else if (animator.GetBool("Letenje") || moveVector.magnitude < 0.38f) motorIdle.pitch = 0.5f;
        else motorIdle.pitch = (moveVector.magnitude * 1.3f);

        
        // ker med kopanjem spreminja rigidbody ga tu spomnim da je se vedno pavza
        if (pause) pauseGame(true);
        // dobi najnovejse podatke o stanju iz skupne skripte o stanjih
        flySpeed = gameObject.GetComponent<Xml>().flySpeed;
        speed        = gameObject.GetComponent<Xml>().speed;
        trenje       = gameObject.GetComponent<Xml>().trenje;
        //kopanjeSpeed = gameObject.GetComponent<Xml>().kopanjeSpeed;
        kopanjeSpeed = ((0.0007f * transform.position.y + 1) * (((transform.GetComponent<Xml>().selectedDrill * 0.125f) + (transform.GetComponent<Xml>().selectedEngine * 0.125f)) / 2) * maxKopanje);
        


        if (!premikanje) kopac.velocity = new Vector2(0, -5);

        // joystic vector for direction
        moveVector = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical"));

        // preklice animacijo prasenja ko ne koplje
        if (!kopanje) {
            if (sideDust.isPlaying) sideDust.Stop();
            if (bottomDust.isPlaying) bottomDust.Stop();
        } 
        // v primeru da koplje izkljuci strani kopanja ki niso potrebna
        else if (kopanjeSmer == 0) sideDust.Stop();
        else { bottomDust.Stop(); }


        // aktiviranje dinamita
        if (Input.GetKeyDown(KeyCode.R)) {
            dimamitExplosion();
            stanjeCanvas.GetComponent<Ratio>().updateSpecialElementNumber();
        }

        // aktiviranje elektricne bombe
        if (Input.GetKeyDown(KeyCode.E)) {
            electricExplosion();
            stanjeCanvas.GetComponent<Ratio>().updateSpecialElementNumber();
        }

        // preveri ali je v stanju kopanja in sprozi ustrezne animacije
        if (kopanje) {
            gameObject.GetComponent<Xml>().fuelState -= 0.25f * Time.deltaTime;
            izpuh.startColor = new Color32(0, 0, 0, 255);
            izpuh.emissionRate = 25;
            izpuh.startSpeed = 2;

        }
        else {
            izpuh.startColor = new Color32(255, 255, 255, 255);
            izpuh.emissionRate = 10;
            izpuh.startSpeed = 1.3f;
        }

        // letenje animacija
        if (bottom.collider != null || kopanje) {
            kopac.drag = trenje;
            animator.SetBool("Letenje", false);
            animator.SetBool("KopanjeSide", true);
            // ustavi zvok letenja
            if (helicopterSound.isPlaying) helicopterSound.Stop();
        }
        else {
            kopac.drag = 0.3f;
            animator.SetBool("Letenje", true);
            animator.SetBool("KopanjeSide", false);
            // preveri ali zvok za letenje ze igra
            if (!helicopterSound.isPlaying && !pause) helicopterSound.Play();
        }


        // ustreli zarke in si zapomni ce je bilo na kateri strani kaj zadeto
        bottom = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.37f), new Vector2(0, -1.2f), 0.0001f);
        right = Physics2D.Raycast(new Vector2(transform.position.x + 0.5f, transform.position.y), new Vector2(0.5f, 0), 0.2f);
        left = Physics2D.Raycast(new Vector2(transform.position.x - 0.5f, transform.position.y), new Vector2(-0.5f, 0), 0.2f);

        // detekcija kdaj se je kopac zaletel v tla
        if (kopac.velocity.y < -10 && bottom.collider != null) {
            // odbijem mu zdravje
            kopac.GetComponent<Xml>().health += kopac.velocity.y;
            // nastavim njegov velocity po y osi na 0
            kopac.velocity = new Vector2(kopac.velocity.x, 0);

            // rdeca animaicija
            damageAnimation();
            // predvaja zvok ko se udari v tla, predvaja le ko se ima zdravje
            if(kopac.GetComponent<Xml>().health > 0) hitSound.Play();
        }

        // ce kopacu zmanjka zdravja ali goriva, prozim eksplozijo, ki kopaca unici
        if (dead && !kopanje) {
            // nastavi bencin in zdravje na polovico
            gameObject.GetComponent<Xml>().fuelState = gameObject.GetComponent<Xml>().fuelCapacity[gameObject.GetComponent<Xml>().selectedFuel] / 2;
            gameObject.GetComponent<Xml>().health = gameObject.GetComponent<Xml>().hullHealth[gameObject.GetComponent<Xml>().selectedHull] / 2;
            destroyMiner();
        }

        // preveri ali mu da stanje kot mrtev
        if (gameObject.GetComponent<Xml>().fuelState <= 0 || gameObject.GetComponent<Xml>().health <= 0) dead = true;

        
        
        // v primeru kopanja klice animacijo za kopanje
        if (kopanje) kopanjeAnimacija();
        
        // up
        if ((Input.GetKey(KeyCode.UpArrow) || moveVector.y > 0.4) && !kopanje && premikanje) {

            // doda force, ki dvigne kopaca
            kopac.AddForce(transform.up * 100);
            // omeji hitrost letenja glede na to ce je sel skozi limito
            
            // preveri ali uporabljam tipkovnico 
            if (kopac.velocity.y > flySpeed * moveVector.y * (Time.deltaTime / 0.0166666666f) && Input.GetKey(KeyCode.UpArrow)) {

                // ko pritisnem shift za hitrejse letenje
                if (Input.GetKey(KeyCode.LeftShift)) {
                    kopac.velocity = new Vector2(kopac.velocity.x, kopac.velocity.normalized.y * flySpeed);
                }
                else {
                    kopac.velocity = new Vector2(kopac.velocity.x, kopac.velocity.normalized.y * flySpeed/2);
                }
               

            }

            // preveri ali uporabljam joystic
            else if (kopac.velocity.y > flySpeed * moveVector.y * (Time.deltaTime / 0.0166666666f) && moveVector.y > 0.9f){
                kopac.velocity = new Vector2(kopac.velocity.x, kopac.velocity.normalized.y * flySpeed * moveVector.y);
                
            }
            // do dolocene pozicije palcke je kontrola bolj precizna in pocasna za lazji nadzor
            else if (kopac.velocity.y > 2 && moveVector.y < 0.9f) {
                kopac.velocity = new Vector2(kopac.velocity.x, kopac.velocity.normalized.y * 2 * moveVector.y);
                
            }
        }

        // down
        if ((Input.GetKey(KeyCode.DownArrow) || moveVector.y < -0.5f) && !kopanje && premikanje && !dead) {
            if (bottom.collider != null && (bottom.collider.tag == "ground" || bottom.collider.tag == "mina")) {
                
                // premakne se tocno na sredino rezanega objekta po x osi
                transform.position = new Vector2(bottom.collider.gameObject.transform.position.x, transform.position.y);
                kopani = bottom.collider.gameObject;
                kopani.GetComponent<BoxCollider2D>().enabled = false;
                
                
                // za animacijo kopanja
                kopanjeSmer = 0;
                
                kopanje = true;
                animator.SetBool("KopanjeDown", true);
                animator.SetBool("Shaking", true);
                animator.SetBool("KopanjeSide", false);

            }
        }

        // premik desno
        if (((Input.GetKey(KeyCode.RightArrow) || moveVector.x > 0.1) && !kopanje) && premikanje && !dead) {
            
            // preveri ali ima prosto pot da se premakne, ter ce ima pred sabo stavbo ker ima collider za triger in ga raycast zazna zato ga ignoriram
            if (right.collider == null || right.collider.tag == "Building" || right.collider.tag == "air") {
                kopac.velocity = new Vector2(speed * moveVector.x * (Time.deltaTime / 0.0166666666f), kopac.velocity.y);
                if (Input.GetKey(KeyCode.RightArrow)) kopac.velocity = new Vector2(speed * (Time.deltaTime / 0.0166666666f), kopac.velocity.y);
            }
            
            transform.rotation = Quaternion.Euler(0.0f, 180f, 0.0f);

            // ali ima na desni kak objekt ki ga lahko unici, pogoj je da je na desni objekt in da se dotika tal
            if (right.collider != null && (right.collider.tag == "ground" || right.collider.tag == "mina") && ((moveVector.x > 0.9f || Input.GetKey(KeyCode.RightArrow) && bottom.collider != null) || Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.Space)) ) {
                // postavi ga lepo na sredio
                transform.position = getPosition();
                kopani = right.collider.gameObject;
                kopani.GetComponent<BoxCollider2D>().enabled = false;
                // za animacijo kopanja
                kopanjeSmer = 2;
                kopanje = true;
                animator.SetBool("KopanjeSide", true);
                animator.SetBool("Shaking", true);
            }

        }
        // premik levo
        if ((Input.GetKey(KeyCode.LeftArrow) || moveVector.x < -0.1f) && !kopanje && premikanje && !dead) {
            // preveri ali ima prosto pot da se premakne, ter ce ima pred sabo stavbo ker ima collider za triger in ga raycast zazna zato ga ignoriram
            if (left.collider == null || left.collider.tag == "Building" || left.collider.tag == "air") {
                kopac.velocity = new Vector2(speed * moveVector.x * (Time.deltaTime / 0.0166666666f), kopac.velocity.y);
                if(Input.GetKey(KeyCode.LeftArrow)) kopac.velocity = new Vector2(-speed * (Time.deltaTime / 0.0166666666f), kopac.velocity.y);

            }

            transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            

            // ali ima na levi kak objekt ki ga lahko unici, pogoj je da je na levi objekt in da se dotika tal
            if (left.collider != null  && (left.collider.tag == "ground" || left.collider.tag == "mina") && ((moveVector.x < -0.9f || Input.GetKey(KeyCode.LeftArrow) && bottom.collider != null) || Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.Space)) ) {
                // postavi ga lepo na sredio
                transform.position = getPosition();
                kopani = left.collider.gameObject;
                kopani.GetComponent<BoxCollider2D>().enabled = false;
                // za animacijo kopanja
                kopanjeSmer = 1;
                kopanje = true;
                animator.SetBool("KopanjeSide", true);
                animator.SetBool("Shaking", true);
            }

        }

        
    }

    /*  Pomembne spremenljivke
        kopanjeSmer
            0 - bottom
            1 - left
            2 - right
        kopani -Gameobject
      
     */

    public void kopanjeAnimacija() {
        
        // prekine delovanje fizike
        kopac.simulated = false;
        // kopanje navzdol
        if (kopanjeSmer == 0) {
            if (transform.position.y > kopani.transform.position.y - 0.15f) {
                transform.Translate(new Vector2(0, -kopanjeSpeed * 0.01f * (Time.deltaTime / 0.0166666666f)));
                kopani.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1,(transform.position.y + 0.15f - kopani.transform.position.y));
                if (!bottomDust.gameObject.active) bottomDust.gameObject.SetActive(true);
                animator.SetBool("KopanjeSide", false);
                if (bottomDust.isStopped) bottomDust.Play(); 
            }
            else izkopanOperacije();
        }

        // kopanje levo
        if (kopanjeSmer == 1) {
            if (transform.position.x > kopani.transform.position.x) {
                transform.Translate(new Vector2(-kopanjeSpeed * 0.01f * (Time.deltaTime / 0.0166666666f), 0));
                kopani.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, (transform.position.x - kopani.transform.position.x));
                if (sideDust.isStopped) sideDust.Play();
                if (!sideDust.gameObject.active) sideDust.gameObject.SetActive(true);
            }
            else izkopanOperacije();
        }

        // kopanje desno
        if (kopanjeSmer == 2) {
            if (transform.position.x < kopani.transform.position.x) {
                transform.Translate(new Vector2(-kopanjeSpeed * 0.01f * (Time.deltaTime / 0.0166666666f), 0));
                kopani.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, (kopani.transform.position.x - transform.position.x));
                if (sideDust.isStopped) sideDust.Play();
                if (!sideDust.gameObject.active) sideDust.gameObject.SetActive(true);
            }
            else izkopanOperacije();
        }

    }

    // ko element izkoplje izvede te operacije
    public void izkopanOperacije() {
        // pozicijo izkopanega elementa da v listo
        gameObject.GetComponent<Xml>().destroyListAdd(kopani.transform.position);
        // doda element v zalogovnik
        dodajElement(kopani.ToString());
        // izpise se text nad minerjem
        izpisiText(kopani);
        // preveri ali je sel cez mino
        if (kopani.tag == "mina") minerMina();
        Destroy(kopani);
        kopanje = false;
        // omogoci fiziko
        if (!pause) kopac.simulated = true;
        animator.SetBool("Shaking", false);
        // smeri kopanje da na false
        animator.SetBool("KopanjeDown", false);
        animator.SetBool("KopanjeSide", false);
    }



    // vrne pozicijo v katerem kvadratu se nahaja
    public Vector2 getPosition() {
        int x = (int)(transform.position.x + 0.5f);
        int y = (int)(transform.position.y - 0.5f);
        return new Vector2(x,y - 0.16f);
    }

    // preveri kateri elementi so primerni za dodajanje in ce ima se zadosti prostora
    public void dodajElement(String s) {
        // preveri ali element vsebuje stevilko
        if (s.Substring(0, 1) == "_") {
            int number = Int32.Parse(s.Split('_')[1]);
            // preveri da stevilka ni neveljavna
            if (!(number == 1 || number == 0 || number == 14 || number == 17)) {
                // doda element v zalogovnik
                gameObject.GetComponent<Xml>().addElement(number);
                
            }
        }
    }

    public void izpisiText(GameObject k) {
        // preveru ali je zalogovnik poln
        if (gameObject.GetComponent<Xml>().shramba_elementi() < gameObject.GetComponent<Xml>().storageCapacity[gameObject.GetComponent<Xml>().selectedStorage]) {
            string number = k.ToString().Split('_', '(')[1];

            if (number != "1" && k.ToString().Split('_', '(').Length != 3) {
                Instantiate(mineralText, kopani.transform.position, Quaternion.identity).GetComponent<MineralName>().setText(k.ToString().Split('_', '(')[2]);
                // zvok ob dodanem elemetu
                softBeep.Play();
            }
        } else {
            Instantiate(mineralText, kopani.transform.position, Quaternion.identity).GetComponent<MineralName>().setText("Full");
            // predvaja zvok za poln zalogovnik
            fullBeep.Play();

        }

    }

    // skrije minerja in kreira animacijo ki ga rastreli
    public void destroyMiner() {
        // shrani elemente ki jih bo izgubil
        //shrambaRewardAdd = transform.GetComponent<Xml>().shramba;
        // preveri ali ima elemente da lahko poda zahtevo za reklamo
        canPlayAdd = (transform.GetComponent<Xml>().shramba_elementi() > 0);

        // predvaja zvok
        destroySound.Play();
        // prekine particle system kopanja v primeru da takrat koplje
        sideDust.Stop();
        bottomDust.Stop();
        // onemogoci joystic
        premikanje = false;
        // iznici vse sile na kopaca
        kopac.velocity = new Vector2(0, 0);
        // onemogoci premikanje kamere
        kamera.GetComponent<Kamera>().premikanje = false;
        // naredi instance s ekspozijo
        Instantiate(destroyAnimation, transform.position, Quaternion.identity);
        
        // premakne minerja na zacetno pozicijo
        transform.position = new Vector2(19, 2);
        dead = false;

    }

    // prikaze minerja in ga postavi na zaceten polozaj
    public void spawnMiner() {
        
        // omogoci premikanje kamere
        kamera.GetComponent<Kamera>().premikanje = true;
        // resetira izrisovalnik tal
        transform.GetComponent<GroundGenerator>().resetGround();

        // vse izkopane elemente izbrise
        gameObject.GetComponent<Xml>().shramba = new Dictionary<int, int>();
        // omogoci premikanje minerja
        premikanje = true;
        //preveri ali lahko predvaja ad -> vprasa ga ce zeli rewarded add
        //if(canPlayAdd && adManager.GetComponent<RewardedAd>().rewardBasedVideo.IsLoaded()) objController.GetComponent<MessageManager>().rewardedAdd();

    }

    // vrne izgubljene elemente
    public void returnElements() {
        gameObject.GetComponent<Xml>().shramba = shrambaRewardAdd;
    }

    

    // naredi eksplozijo za dinamit
    public void dimamitExplosion() {
        // preveri ali ima sploh dinamit
        if (transform.GetComponent<Xml>().specialElements[0] <= 0) {
            // predvaja zvok 
            fullBeep.Play();
            return;
        }
        // ce ima zadosti elementov odsteje
        transform.GetComponent<Xml>().specialElements[0]--;
        // izracuna zaceten blok za X smer
        int xPos = ((int)(transform.position.x + 0.5)) - 1;
        // dobi pozicijo za Y smer
        int yPos = (int)(transform.position.y - 0.5) - 1;


        // naredim dvojno zanko za 3x3 eksplozijo
        // stolpci
        for (int y = yPos; y > yPos + -3; y--) {
            // vrstice
            for (int x = xPos; x < xPos + 3; x++) {
                Vector2 tocka = new Vector2(x, y);

                // preveri ali pod to pozicijo sploh obstaja objekt
                if(Physics2D.OverlapCircle(tocka, 0.1f) != null) {
                    // dobi objekt pod to pozicijo
                    GameObject objekt = Physics2D.OverlapCircle(tocka, 0.1f).gameObject;

                    // preveri ali ga lahko unici
                    if (objekt.tag == "ground" || objekt.tag == "ovira" || objekt.tag == "mina"){
                        // pozicijo ekxplodiranega elementa da v listo
                        gameObject.GetComponent<Xml>().destroyListAdd(objekt.transform.position);
                        Destroy(objekt);
                    }

                    if (objekt.tag == "boss") {
                        print("Boss hitted");
                        boss.GetComponent<Boss_1>().enemyHealth -= 5;
                        
                    }
                }
                // kreira eksplozijo
                Instantiate(dinamit, tocka, Quaternion.identity);
                bombSound.Play();
            }
        }
    }

    // naredi eksplozijo za elektricno bombo
    public void electricExplosion() {
        // preveri ali ima bombo na zalogi
        if (transform.GetComponent<Xml>().specialElements[1] <= 0) {
            // predvaja zvok 
            fullBeep.Play();
            return;
        }
        // ce ima zadosti elementov odsteje za 1
        transform.GetComponent<Xml>().specialElements[1]--;

        // naredi animacijo za elektricno eksplozijo na trenutnem mestu
        Instantiate(electricBomb, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
        ebombSound.Play();
        // dobi listo vseh objektov v radiju
        foreach (Collider2D col in Physics2D.OverlapCircleAll(transform.position, 6)) {
            // preveri ali ga lahko unici
            if (col.gameObject.tag == "ovira" || col.gameObject.tag == "mina") {
                // kreira eksplozijo
                Instantiate(dinamit, col.gameObject.transform.position, Quaternion.identity);
                gameObject.GetComponent<Xml>().destroyListAdd(col.gameObject.transform.position);
                // unici objekt
                Destroy(col.gameObject);
                destroySound.Play();
            }

            // ko napade boosa
            if (col.gameObject.tag == "boss" && transform.position.y < -1000) {
                boss.GetComponent<Boss_1>().enemyHealth -= 20;
                // animacija dinamita se spremeni v posevno lego
                for(int i = 0; i < 4; i++)
                    Instantiate(dinamit, new Vector2(col.gameObject.transform.position.x, col.gameObject.transform.position.y + i), Quaternion.identity);            
            }
        }
    }

    // ko gre skozi mino se klice ta funkcija
    public void minerMina() {
        // naredi eksplozijo na mestu kjer se nahaja miner
        Instantiate(dinamit, transform.position, Quaternion.identity);
        // minerju vzame zdravje
        gameObject.GetComponent<Xml>().health -= 5;
        // rdeca animaicija
        damageAnimation();
        bombSound.Play();


    }

    // animacija rdecega zaslona ob udarcu
    public void damageAnimation() {
        // rdec zaslon animacija
        Instantiate(destroyColorScreen, transform.position, Quaternion.identity);       
    }

    public void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "air") {
            // doda element v tabelo izkopanih, ker ob nasledenjem generiranju mape tam nebo zraka
            gameObject.GetComponent<Xml>().destroyListAdd(collision.gameObject.transform.position);
            // unici zrak 
            Destroy(collision.gameObject);
        }
    }

    // za pavziranje igre
    public void pauseGame(bool stop) {
        if (stop) {
            // preveri ali koplje
            if (kopanje) {
                premikanje = false;
                pause = true;
            }
            // ce ne koplje onemogoci fiziko
            else {
                kopac.simulated = false; // prekine fiziko
                helicopterSound.Stop(); 
                pause = true;
                premikanje = false;
                // prepove porabo goriva
                transform.GetComponent<Xml>().useFuel = false;
            }
            
        }
        // omogoci fiziko in prekine pavzo
        else {
            kopac.simulated = true;
            pause = false;
            premikanje = true;
            // omogoci porabo goriva
            transform.GetComponent<Xml>().useFuel = true;
        }
    }


    public void addFuel() {
        // kolicina dodanega goriva
        int fuel = 30;

        // preveri ali ima polno goriva, v tem primeru prekine funkcijo in na to opozori igralca
        if (transform.GetComponent<Xml>().fuelState >= (float)transform.GetComponent<Xml>().fuelCapacity[transform.GetComponent<Xml>().selectedFuel] - 0.5f) {
            Instantiate(mineralText, transform.position, Quaternion.identity).GetComponent<MineralName>().setText("Fuel tank full");
            fullBeep.Play();
            return;
        }

        // preveri ali lahko doda gorivo
        if (transform.GetComponent<Xml>().specialElements[2] <= 0) {
            // predvaja zvok 
            fullBeep.Play();
            return;
        }
        // ce ima element za gorivo odsteje 1
        transform.GetComponent<Xml>().specialElements[2]--;
        
        // preveri ali bo slo cez mejo
        if (transform.GetComponent<Xml>().fuelState + fuel > transform.GetComponent<Xml>().fuelCapacity[transform.GetComponent<Xml>().selectedFuel]) {
            transform.GetComponent<Xml>().fuelState = transform.GetComponent<Xml>().fuelCapacity[transform.GetComponent<Xml>().selectedFuel];
        }
        else {
            // doda 5 litrov goriva
            transform.GetComponent<Xml>().fuelState += fuel;     
        }
        // izpise da sem dodal gorivo
        Instantiate(mineralText, transform.position, Quaternion.identity).GetComponent<MineralName>().setText("Added fuel");
        // predvaja zbok za gorivo
        addFuelSound.Play();

    }

    // za teleportiranje na vrh
    public void teleport() {
        
        // preveri ali ga lahko teleportira
        if (transform.GetComponent<Xml>().specialElements[3] <= 0) {
            // predvaja zvok 
            fullBeep.Play();
            return;
        }

        // ce je na povrsju ga ne teleportira
        if (transform.position.y > 0) {
            //Instantiate(mineralText, transform.position, Quaternion.identity).GetComponent<MineralName>().setText("You are already on surface");
            GameObject msg = Instantiate(mineralText, transform.position, Quaternion.identity);
            msg.GetComponent<MineralName>().setText("You are already on surface");
            msg.GetComponent<MineralName>().barva = new Color32(0,0,0,255);
            fullBeep.Play();
            return;
        }
        

        
        // predvaja zvok za teleportacijo
        ebombSound.Play();
        // ce ima element za teleport odsteje 1
        transform.GetComponent<Xml>().specialElements[3]--;
        // resetira izrisovalnik tal
        transform.position = new Vector2(19,1);
        kopac.velocity = new Vector2(0, 0);
        transform.GetComponent<GroundGenerator>().resetGround();
        Instantiate(teleportEffect, new Vector2(19, 1), Quaternion.identity);

    }

    // za dodajanje zdravja
    public void addHealth() {
        // kolicina dodanega zdravja
        int health = 60;

        // preveri ce je zdravje ze polno, v tem primeru prekine izvajanje funkcije
        if (transform.GetComponent<Xml>().health == transform.GetComponent<Xml>().hullHealth[transform.GetComponent<Xml>().selectedHull]) {
            Instantiate(mineralText, transform.position, Quaternion.identity).GetComponent<MineralName>().setText("Health full");
            fullBeep.Play();
            return;
        }
       
        // preveri ali sploh ima zdravje za dodat
        if (transform.GetComponent<Xml>().specialElements[4] <= 0) return;

        // ce ima element za zdravje odsteje 1
        transform.GetComponent<Xml>().specialElements[4]--;

        // preveri ali bo slo cez mejo
        if (transform.GetComponent<Xml>().health + health > transform.GetComponent<Xml>().hullHealth[transform.GetComponent<Xml>().selectedHull]) {
            transform.GetComponent<Xml>().health = transform.GetComponent<Xml>().hullHealth[transform.GetComponent<Xml>().selectedHull];
        }
        else {
            // doda 5 litrov goriva
            transform.GetComponent<Xml>().health += health;
        }
        // izpise da sem dodal gorivo
        Instantiate(mineralText, transform.position, Quaternion.identity).GetComponent<MineralName>().setText("Added health");
        // predvaja zbok za gorivo
        addFuelSound.Play();
    }

}
