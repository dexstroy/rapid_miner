﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Text;
using System.IO;
using System;
using UnityEngine.UI;

public class Xml : MonoBehaviour {
    public  float flySpeed     = 6;
    public  float speed        = 5;
    public  float trenje       = 1.6f;
    public  float kopanjeSpeed = 3;
    public  float health       = 15;
    public  int money          = 120;
    public float fuelState     = 3;
    public float soundLevel = 1;

    // izbrani elementi
    public int selectedDrill    = 1;
    public int selectedEngine   = 1;
    public int selectedFuel     = 1;
    public int selectedHull     = 1;
    public int selectedRadiator = 1;
    public int selectedStorage  = 1;


    // mnozica kupljenih elementov
    public HashSet<int> boughtDrill = new HashSet<int>();
    public HashSet<int> boughtEngine = new HashSet<int>();
    public HashSet<int> boughtFuel = new HashSet<int>();
    public HashSet<int> boughtHull = new HashSet<int>();
    public HashSet<int> boughtRadiator = new HashSet<int>();
    public HashSet<int> boughtStorage = new HashSet<int>();

    //Lastnosti za posamezne elemente iz store
    public Dictionary<int, float> fuelCapacity = new Dictionary<int, float>() { {1,10}, { 2, 15 }, { 3, 25 }, {4,35}, { 5, 50 }, { 6, 70 }, {7,100}, { 8, 125 } };
    public Dictionary<int, float> storageCapacity = new Dictionary<int, float>() { { 1, 8 }, { 2, 12 }, { 3, 18 }, { 4, 25 }, { 5, 34 }, { 6, 42 }, { 7, 50 }, { 8, 60 } };
    public Dictionary<int, float> hullHealth = new Dictionary<int, float>() { { 1, 15 }, { 2, 25 }, { 3, 35 }, { 4, 50 }, { 5, 80 }, { 6, 120 }, { 7, 180 }, { 8, 250 } };
    public Dictionary<int, float> radiatorDepth = new Dictionary<int, float>() { { 1, 1250 }, { 2, 2500 }, { 3, 3750 }, { 4, 5000 }, { 5, 6250 }, { 6, 7500 }, { 7, 8750 }, { 8, 11000 } };


    // lista v kateri so vse pozicije kjer ni vec elementov, ker so izkopani
    public List<Vector2> destroyList = new List<Vector2>();
    // slovar vektorjev, ki se kliče ob sprotnem generiranju mape za bolj ucinkovito lociranje pozuicij ki morajo biti izbrisane
    public Dictionary<int, List<Vector2>> destroyDict = new Dictionary<int, List<Vector2>>();

    // slovar v katerem so izkopani elementi ki se nahajajo v shrambi
    public Dictionary<int, int> shramba = new Dictionary<int, int>();

    // za enkraten prikaz sporocil
    public Dictionary<int, int> messages = new Dictionary<int, int>();

    // posebni elementi kot so bomba, teleporter itd
    public Dictionary<int, int> specialElements = new Dictionary<int, int>();

    static readonly string imeDatoteke = "data.xml";
    public string path = "";

    public bool useFuel;

    // ko baza prebere vse podatke je true
    public bool loaded = false;

    // ko ob zaprtju nesme shraniti podatkov, to se zgodi ko te boss ubije 
    public bool noSave = true;

    // izrisuje merilnike, dobi ukaz kdaj lahko izrise, da je baza ze nalozena
    public GameObject ratio;

    // za izracun timeplay
    public DateTime startTime;

    // Use this for initialization
    void Start() {
        
        // vzame trenutni cas da potem izracuna cas igranja
        startTime = DateTime.UtcNow;

        path = Path.Combine(Application.persistentDataPath, imeDatoteke);
        print(path);
        
        // kreira base XML dokument v primeru da ne obstaja
        if (!File.Exists(path)) createXmlTemplate();

        // kreira nastavitveni setting xml v primeru da ne obstaja
        createSettingTemplate();

        readXML();
        // napolni slovar z tockami ki morajo biti izbrisane se iz prejsnje runde igranja, zaradi generiranja mape
        
        // prebere setting xml in zapise vse potrebne vrednosti
        readSettingXml();

        fillDict();
        InvokeRepeating("updateState", 2, 1);

        // ko prebere xml file nastavi soundLevel za vse zvoke od minerja
        setSoudLevel();

        // ko opravi vse kriticne operacije sele dovljuje shranjevanje
        noSave = false;
        
    }


    // prebrere XML file in dodeli podatke
    public void readXML() {
        print("Read_XML");
        XmlDocument bralec = new XmlDocument();
        //bralec.Load("C:/Users/computer/AppData/LocalLow/DefaultCompany/miner2/data.xml");
        bralec.Load(path);
        flySpeed = float.Parse(bralec.SelectSingleNode("miner/stanje/flySpeed").InnerText);
        speed = float.Parse(bralec.SelectSingleNode("miner/stanje/speed").InnerText);
        trenje = float.Parse(bralec.SelectSingleNode("miner/stanje/trenje").InnerText);
        kopanjeSpeed = float.Parse(bralec.SelectSingleNode("miner/stanje/kopanjeSpeed").InnerText);
        health = float.Parse(bralec.SelectSingleNode("miner/stanje/health").InnerText);
        money = Int32.Parse(bralec.SelectSingleNode("miner/stanje/money").InnerText);
        fuelState = float.Parse(bralec.SelectSingleNode("miner/stanje/fuelState").InnerText);
        

        // izbrane komponente
        selectedDrill    = Int32.Parse(bralec.SelectSingleNode("miner/selectedElement/drill").InnerText);
        selectedEngine   = Int32.Parse(bralec.SelectSingleNode("miner/selectedElement/engine").InnerText);
        selectedFuel     = Int32.Parse(bralec.SelectSingleNode("miner/selectedElement/fuel").InnerText);
        selectedHull     = Int32.Parse(bralec.SelectSingleNode("miner/selectedElement/hull").InnerText);
        selectedRadiator = Int32.Parse(bralec.SelectSingleNode("miner/selectedElement/radiator").InnerText);
        selectedStorage  = Int32.Parse(bralec.SelectSingleNode("miner/selectedElement/storage").InnerText);

        // kupljene komponente
        boughtDrill    = stringToSet(bralec.SelectSingleNode("miner/boughtElements/drill").InnerText);
        boughtEngine   = stringToSet(bralec.SelectSingleNode("miner/boughtElements/engine").InnerText);
        boughtFuel     = stringToSet(bralec.SelectSingleNode("miner/boughtElements/fuel").InnerText);
        boughtHull     = stringToSet(bralec.SelectSingleNode("miner/boughtElements/hull").InnerText);
        boughtRadiator = stringToSet(bralec.SelectSingleNode("miner/boughtElements/radiator").InnerText);
        boughtStorage  = stringToSet(bralec.SelectSingleNode("miner/boughtElements/storage").InnerText);

        // shrani elemente iz shrambe
        shramba = stringToDict(bralec.SelectSingleNode("miner/stanje/shramba").InnerText);

        // vrednosti prikazov sporocil
        messages = stringToDict(bralec.SelectSingleNode("miner/other/messages").InnerText);

        specialElements = stringToDict(bralec.SelectSingleNode("miner/other/elements").InnerText);

        

        // destroyList napolni s koordinatami glede na podan ni koordinat
        stringToList(bralec.SelectSingleNode("miner/izkopani").InnerText);

        // tako da ostali programi vejo kdaj se je baza nalozila
        loaded = true;
        ratio.GetComponent<Ratio>().updateSpecialElementNumber();
        // sedaj ko se je vse nalozilo lahko izbrisem iskopane
        uniciIzkopane();

    }

    // prebere se nastavitve za zvok
    public void readSettingXml() {

        // sestavi path
        string pot = Path.Combine(Application.persistentDataPath, "settings.xml");

        XmlDocument bralec = new XmlDocument();
        bralec.Load(pot);
        // prebere nivo zvoka
        soundLevel = float.Parse(bralec.SelectSingleNode("settings/sound").InnerText);

    }

    // ko se igra zapre se shranijo novi podatki v XML
    public void OnApplicationQuit() {
        // v primeru da nesme shraniti podatkov ob izhodu
        if (!noSave) saveXML();
    }

    // za mobilne telefone da shrani 
    public void OnApplicationPause(bool pause) {

        // ko je pavzirana
        if (pause) {
            print("Pavzirana");
            // v primeru da nesme shraniti podatkov ob izhodu
            if (!noSave) saveXML();
        }
        // ko se vrne iz pavze
        else {
            print("Resume");
            // resetira cas na novo
            startTime = DateTime.UtcNow;
        }
        
        
        
    }

    // shrani spremenljivke nazaj v XML file
    public void saveXML() {
        print("Klican save");
        XmlDocument bralec = new XmlDocument();
        //bralec.Load("C:/Users/computer/AppData/LocalLow/DefaultCompany/miner2/data.xml");
        bralec.Load(path);

        // obnasanje stroja
        bralec.SelectSingleNode("miner/stanje/flySpeed").InnerText = flySpeed.ToString();
        bralec.SelectSingleNode("miner/stanje/speed").InnerText = speed.ToString();
        bralec.SelectSingleNode("miner/stanje/trenje").InnerText = trenje.ToString();
        bralec.SelectSingleNode("miner/stanje/kopanjeSpeed").InnerText = kopanjeSpeed.ToString();

        // stanje stroja
        bralec.SelectSingleNode("miner/stanje/health").InnerText = health.ToString();
        bralec.SelectSingleNode("miner/stanje/money").InnerText = money.ToString();
        bralec.SelectSingleNode("miner/stanje/fuelState").InnerText = fuelState.ToString();

        // izbrani elementi
        bralec.SelectSingleNode("miner/selectedElement/drill").InnerText = selectedDrill.ToString();
        bralec.SelectSingleNode("miner/selectedElement/engine").InnerText = selectedEngine.ToString();
        bralec.SelectSingleNode("miner/selectedElement/fuel").InnerText = selectedFuel.ToString();
        bralec.SelectSingleNode("miner/selectedElement/hull").InnerText = selectedHull.ToString();
        bralec.SelectSingleNode("miner/selectedElement/radiator").InnerText = selectedRadiator.ToString();
        bralec.SelectSingleNode("miner/selectedElement/storage").InnerText = selectedStorage.ToString();

        // kupljeni elementi
        bralec.SelectSingleNode("miner/boughtElements/drill").InnerText = setToString(boughtDrill);
        bralec.SelectSingleNode("miner/boughtElements/engine").InnerText = setToString(boughtEngine);
        bralec.SelectSingleNode("miner/boughtElements/fuel").InnerText = setToString(boughtFuel);
        bralec.SelectSingleNode("miner/boughtElements/hull").InnerText = setToString(boughtHull);
        bralec.SelectSingleNode("miner/boughtElements/radiator").InnerText = setToString(boughtRadiator);
        bralec.SelectSingleNode("miner/boughtElements/storage").InnerText = setToString(boughtStorage);

        // izkopani elementi
        bralec.SelectSingleNode("miner/izkopani").InnerText = listToString();

        // elementi v shrambi
        //bralec.SelectSingleNode("miner/stanje/shramba").InnerText = dictToString(shramba);

        // shrani prikaz sporocil
        bralec.SelectSingleNode("miner/other/messages").InnerText = dictToString(messages);

        // shrani stevilo posebnih elementov sporocil
        bralec.SelectSingleNode("miner/other/elements").InnerText = dictToString(specialElements);

        // pristejem cas igranja
        
        // vzame trenutni cas in ga odsteje od zacetnega
        DateTime now = DateTime.UtcNow;
        int timePlay = Int32.Parse(bralec.SelectSingleNode("miner/other/timePlay").InnerText) + (int)now.Subtract(startTime).Seconds;
        //int timePlay = Int32.Parse(bralec.SelectSingleNode("miner/other/timePlay").InnerText) + (int)Time.timeSinceLevelLoad;
        bralec.SelectSingleNode("miner/other/timePlay").InnerText = timePlay.ToString();

        //bralec.Save("C:/Users/computer/AppData/LocalLow/DefaultCompany/miner2/data.xml");
        bralec.Save(path);

        print("Save End");
    }

    public void createXmlTemplate() {
        print("Create XML template");
        // ce template za obstaja ga izbrise
        if (File.Exists(path)) File.Delete(path);

        // pisalec za izdelavo XML datoteke
        XmlTextWriter pisalec = new XmlTextWriter(path, Encoding.UTF8);
        // za zamikanje vrstic in boljso preglednost
        pisalec.Formatting = Formatting.Indented;

        pisalec.WriteStartElement("miner");
        pisalec.WriteStartElement("stanje");

        pisalec.WriteStartElement("flySpeed");
        pisalec.WriteString("6");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("speed");
        pisalec.WriteString("5");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("trenje");
        pisalec.WriteString("1.6");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("kopanjeSpeed");
        pisalec.WriteString("5");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("health");
        pisalec.WriteString("15");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("money");
        pisalec.WriteString("120");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("fuelState");
        pisalec.WriteString("10");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("shramba");
        pisalec.WriteString("");
        pisalec.WriteEndElement();

        pisalec.WriteEndElement(); // </stanje>

        pisalec.WriteStartElement("izkopani");
        pisalec.WriteString("");
        pisalec.WriteEndElement(); // </izkopani>

        pisalec.WriteStartElement("selectedElement");

        pisalec.WriteStartElement("drill");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("engine");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("fuel");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("hull");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("radiator");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("storage");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();



        pisalec.WriteEndElement(); // </selectedElement>

        pisalec.WriteStartElement("boughtElements"); // <boughtElements>

        pisalec.WriteStartElement("drill");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("engine");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("fuel");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("hull");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("radiator");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteStartElement("storage");
        pisalec.WriteString("1");
        pisalec.WriteEndElement();

        pisalec.WriteEndElement(); // </boughtElements>

        // za vse ostale stvari kot so prikazi sporocil itd
        pisalec.WriteStartElement("other");

        // za prikaz sporocil, da se ne ponovijo veckrat
        /*Ima atribute ki si sledijo po vrsti
         * 0 Ko prvic prides v igro
         * 1 Ko ti prvic zmanjka bencina
         * 2 Ko se ti prvic dvigne temperatura
         * 3 Ko prvic obisces fuelPump
         * 4 Ko prvic obisces mineralStore
         * 5 Ko prvic obisces Store
         * 6 Ko prvic obisces Repair shop
         * 7 Ko ti prvic zmanjka prostora v zbiralniku
         */
        pisalec.WriteStartElement("messages");
        pisalec.WriteString("0:0 1:0 2:0 3:0 4:0 5:0 6:0 7:0");
        pisalec.WriteEndElement();

        // za stevilo special elementov
        /*
         * 0 Dynamite
         * 1 Electric bomb
         * 2 fuel
         * 3 teleporter
         * 4 Health
         */
        pisalec.WriteStartElement("elements");
        pisalec.WriteString("0:3 1:3 2:3 3:1 4:3");
        pisalec.WriteEndElement();

        

        pisalec.WriteStartElement("timePlay");
        pisalec.WriteString("0");
        pisalec.WriteEndElement();

        pisalec.WriteEndElement(); // other

        pisalec.WriteEndElement(); // </miner>
        pisalec.Close();
    }



    // za nastavitve ima posebej bazo imenovano settings
    public void createSettingTemplate() {
        print("Create setting template");
        // sestavi path
        string pot = Path.Combine(Application.persistentDataPath, "settings.xml");
        // preveri ali ta template ze obstaja, ce obstaja prekine izvajanje funkcije
        if (File.Exists(pot)) return;

        // XML Writter za pisanje XML datotek
        XmlTextWriter pisalec = new XmlTextWriter(pot, Encoding.UTF8);
        // za zamikanje vrstic in boljso preglednost
        pisalec.Formatting = Formatting.Indented;

        pisalec.WriteStartElement("settings");

        pisalec.WriteStartElement("sound");
        pisalec.WriteString("1");
        pisalec.WriteEndElement(); // </sound>

        pisalec.WriteStartElement("scores");
        pisalec.WriteString("");
        pisalec.WriteEndElement(); // </scores>


        pisalec.WriteEndElement(); // </settings>

        pisalec.Close();

    }

    // listo izkopanih elementov pretvori v String
    public String listToString() {
        string kordinate = "";
        for (int i = 0; i < destroyList.Count; i++) {
            kordinate += destroyList[i].x + ":";
            kordinate += destroyList[i].y + " ";
        }
        return kordinate;
    }

    // iz dobljenega niza koordinat sestavi listo Vectorjev2
    public void stringToList(string elementi) {
        destroyList = new List<Vector2>();

        string[] kordinate = elementi.Split(' ');

        foreach (string kordinata in kordinate) {
            string[] points = kordinata.Split(':');
            // ker je en presledek na koncu ga pri splitanju spravi kot prazen niz, zato preverjam da ni prazen
            if(kordinata != "")destroyList.Add(new Vector2(Int32.Parse(points[0]), Int32.Parse(points[1])));

        }
    }

    // unici vse bloke ki so bili ze izkopani
    // klice jo generator ko izrise vse elemente
    public void uniciIzkopane() {
        foreach (Vector2 tocka in destroyList) {
            //print(tocka);
            // preveri da ni slucajno zadel zrak kar pomeni da vrne null in ce takoj klicem gameobject dobim Exception
            if (Physics2D.OverlapCircle(tocka, 0.1f) != null) {
                Destroy(Physics2D.OverlapCircle(tocka, 0.1f).gameObject);
            }
        }
    }

    // doda izkopani element v shrambo
    public void addElement(int element) {
        // preveri ali ima zadosti prostora
        if (shramba_elementi() >= storageCapacity[selectedStorage]) return;
        //preveri ali element ze obstaja v slovarju
        if (shramba.ContainsKey(element)) {
            shramba[element] += 1;
        }else {
            shramba.Add(element, 1);
        }
    }

    // vrne stevilo elementov v shrambi
    public int shramba_elementi() {
        int st_elemnentov = 0;
        foreach (int kljuc in shramba.Keys) {
            st_elemnentov += shramba[kljuc];
        }
        return st_elemnentov;
    }

    // napolni slovar z vrednostimi ki morajo biti izbrisane pri generiranju mape
    public void fillDict() {
        foreach (Vector2 kordinata in destroyList) {
            // preveri ali kljuc ze obstaja
            if (destroyDict.ContainsKey((int)kordinata.y)) {
                destroyDict[(int)kordinata.y].Add(kordinata);
            } else {
                List<Vector2> t = new List<Vector2>();
                t.Add(kordinata);
                destroyDict.Add((int)kordinata.y, t);
            }
        }
    }

    // unici vse bloke ki so bili ze izkopani v doloceni vrstici
    public void uniciIzkopaneVrstica(int vrstica) {
        // preveri ali slovar vsebuje tocke na tej vrstici
        if (destroyDict.ContainsKey(vrstica)) {
            // unici vse objekte v tej vrstici
            
            foreach (Vector2 tocka in destroyDict[vrstica]) {
                // preveri da ni slucajno zadel zrak kar pomeni da vrne null in ce takoj klicem gameobject dobim Exception
                if (Physics2D.OverlapCircle(tocka, 0.1f) != null) {
                    Destroy(Physics2D.OverlapCircle(tocka, 0.1f).gameObject);
                }
            }
        }
    }

    // mnozico pretvori v string
    public String setToString(HashSet<int> mnozica) {
        String besedilo = "";
        foreach (int element in mnozica) {
            besedilo += element.ToString() + " ";
        }
        // izbrise zadnji presledek
        besedilo = besedilo.Remove(besedilo.Length - 1);

        return besedilo;
    }

    // string v mnozico
    public HashSet<int> stringToSet(String besedilo) {
        HashSet<int> mnozica = new HashSet<int>();
        foreach (string element in besedilo.Split(' ')) {
            mnozica.Add(Int32.Parse(element));
        }
        return mnozica;
    }

    // slovar v string
    public string dictToString(Dictionary<int,int> d) {
        string besedilo = "";
        foreach (int key in d.Keys) {
            besedilo += key.ToString() + ":" + d[key] + " ";
        }
        // izbrise zadnji presledek
        if(besedilo.Length > 0 ) besedilo = besedilo.Remove(besedilo.Length - 1);
        return besedilo;
    }

    public Dictionary<int, int> stringToDict(string besedilo) {
        Dictionary<int, int> slovar = new Dictionary<int, int>();
        string[] list = besedilo.Split(' ');
        
        foreach (string element in list) {
           if(element.Length != 0) slovar.Add(Int32.Parse(element.Split(':')[0]), Int32.Parse(element.Split(':')[1]));
        }
        return slovar;
    }

    // odsteje vsako sekundo fuel za idle delovanje
    void updateState() {
        // ko uporablja gorivo odsteje (takrat ko sem v stavbah ga ne porablja)
        if(useFuel)fuelState -= 0.02f;

        /*
        // ce je bila pauza izvede operacije ko se vrne nazaj
        if (paused) {
            // resetira start time
            startTime = DateTime.UtcNow;
            paused = false;
            print("Start time reset");
        }
        */
    }

    // doda element v destroy listo
    public void destroyListAdd(Vector2 kordinata) {
        destroyList.Add(kordinata);

        // doda element se v slovar za brisanje elementov med ponovnim generiranjem mape
        // preveri ali kljuc ze obstaja
        if (destroyDict.ContainsKey((int)kordinata.y)) {
            destroyDict[(int)kordinata.y].Add(kordinata);
        }
        else {
            List<Vector2> t = new List<Vector2>();
            t.Add(kordinata);
            destroyDict.Add((int)kordinata.y, t);
        }
    }

    // nastavi sound level za vse zvoke v minerju
    public void setSoudLevel() {
        GetComponent<Kopac>().bombSound.volume = soundLevel;
        GetComponent<Kopac>().ebombSound.volume = soundLevel;
        GetComponent<Kopac>().destroySound.volume = soundLevel;
        GetComponent<Kopac>().softBeep.volume = soundLevel;
        GetComponent<Kopac>().fullBeep.volume = soundLevel;
        GetComponent<Kopac>().helicopterSound.volume = soundLevel;
        GetComponent<Kopac>().motorIdle.volume = soundLevel;
        GetComponent<Kopac>().addFuelSound.volume = soundLevel;
        GetComponent<Kopac>().hitSound.volume = soundLevel;
        GetComponent<Kopac>().blagajna.volume = soundLevel;
    }

}
