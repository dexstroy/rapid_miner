﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricBomb : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InvokeRepeating("destroyItself", 4, 1);
    }


    // unici sam sebe
    public void destroyItself() {
        Destroy(gameObject);
    }
}
