﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dinamit : MonoBehaviour {

	// Use this for initialization
	void Start () {
        InvokeRepeating("destroyIstelf", 2, 1);
    }

    // ubjekt eksplozije unici sam sebe po dolocenem casu
    public void destroyIstelf() {
        Destroy(gameObject);
    }
}
