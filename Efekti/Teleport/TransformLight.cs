﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLight : MonoBehaviour {

    // Use this for initialization
    void Start() {
        InvokeRepeating("destroyItself", 3, 1);
    }


    // unici sam sebe
    public void destroyItself() {
        Destroy(gameObject);
    }
}
