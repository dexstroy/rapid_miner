﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddMessage : MonoBehaviour {
    public Text t;
    public Animator anim;
	// Use this for initialization
	void Start () {
	}

    public void setText(string s) {
        t.text = s;
    }

    public void destroyAnimation(bool reklama) {
        if (reklama) {
            print("Predvajaj add");
            GameObject.Find("Ads").GetComponent<RewardedAd>().ShowRewardBasedVideo();
        } 
        else print("Ne predjajaj add");
        anim.SetBool("unici", true);
    }

    public void delete() {
        GameObject.Find("Miner").GetComponent<Kopac>().pauseGame(false);
        Destroy(gameObject);
    }
}
