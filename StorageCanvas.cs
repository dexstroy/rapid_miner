﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageCanvas : MonoBehaviour {
    public GameObject panel;
    // za dostop do baze
    public GameObject miner;
    // panel v kateremu so gumbi
    public GameObject slidePanel;
    // nastavi sirino kvadratka
    int velikost = 0;

	void Start () {
        //panel.SetActive(false);
    }

    public void showPanel(bool b) {
        panel.SetActive(b);
        // uzracuna velikost kvadratka
        velikost = (int)slidePanel.GetComponent<RectTransform>().rect.width / 4;
        slidePanel.GetComponent<GridLayoutGroup>().cellSize = new Vector2(velikost, velikost * 1.3f);

        // klice uodate panel da dobi sveze invormacije
        updatePanel();
    }

    // prikaze ali skrije gumbe glede na to ali je ta mineral v slovarju
    public void updatePanel() {
        // nastavi globino glede na stevilo elementov
        slidePanel.GetComponent<RectTransform>().sizeDelta = new Vector2(0, velikost * 1.3f * Mathf.CeilToInt((float)miner.GetComponent<Xml>().shramba.Count / 4.0f));
        // dobi vse otroke
        foreach (Transform child in slidePanel.transform) {
            // ga skririje ali prikaze glede na to ali slovar vsebuje ta kljuc
            if (miner.GetComponent<Xml>().shramba.ContainsKey(int.Parse(child.name))) {
                child.gameObject.SetActive(true);
                // nastavi stevilo elementov
                child.transform.GetChild(2).GetComponent<Text>().text = miner.GetComponent<Xml>().shramba[int.Parse(child.name)].ToString();
            }
            else {
                child.gameObject.SetActive(false);
            }
        }
    }

    public void removeMineral(int element) {
        // preveri ali element obstaja
        if (!miner.GetComponent<Xml>().shramba.ContainsKey(element)) return;
        // odsteje elementu eno vrednost
        miner.GetComponent<Xml>().shramba[element]--;
        // preveri ali je vrednost enaka 0 in ga odstrani iz slovarja
        if (miner.GetComponent<Xml>().shramba[element] <= 0) miner.GetComponent<Xml>().shramba.Remove(element);
        // klice ponovni izris
        updatePanel();
    }
	
}
