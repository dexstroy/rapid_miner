﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDraw : MonoBehaviour {

   
    // različne vrste rud za kopiranje
    public GameObject[] list;

 

    /* glede na podano listo izrise elemente na zaslon
       podamo mu tudi od kje do kje
       primer from = 0, to = 39; --> izrise 40 vrtic in zacne cisto na zacetku konca pa pri 40 vrstici 
     */
    public void izris(int[,] lista ,int from, int to) {
        print(DateTime.Now + " " + DateTime.Now.Millisecond.ToString());
        // stolpci
        for (int i = from; i <= to; i++) {
            for (int j = 0; j < 32; j++) {
                Instantiate(list[lista[j, i]], new Vector3(j, -i, 0), Quaternion.identity);
                
            }
        }
        print(DateTime.Now + " " + DateTime.Now.Millisecond.ToString());
    } 
}
