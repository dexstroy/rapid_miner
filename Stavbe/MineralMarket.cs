﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MineralMarket : MonoBehaviour {


    public GameObject canvas;
    public Button backButton;

    public Text moneyState; // izpisuje trenutno stanje denarja
    public Text totalValueText;
    

    // za onemogocanje premikanja med obiskom Marketa
    public GameObject miner;

    public GameObject mineralPanel;

    public AudioSource buySound;

    /*
    public Dictionary<int, int> price = new Dictionary<int, int>() {
        {2,40},
        {3,100},
        {4,200},
        {5,400},
        {6,700},
        {7,1000},
        {8,1500},
        {9,2500},
        {10,4000},
        {11,7000},
        {12,12000},
        {13,20000},

        {15,2000},
        {16,2000},
        {17,2000},
        {18,3000},
        {19,10000},
        {20,10000},
        {21,16000},
        {22,16000},
        {23,24000},
        {24,24000}
    };
    */

    public Dictionary<int, int> price = new Dictionary<int, int>() {
        {2,40},
        {3,100},
        {4,200},
        {5,400},
        {6,700},
        {7,1000},
        {8,1500},
        {9,2500},
        {10,4000},
        {11,7000},
        {12,12000},
        {13,20000},

        {15,2000},
        {16,2000},
        {17,2000},
        {18,3000},
        {19,10000},
        {20,10000},
        {21,16000},
        {22,16000},
        {23,24000},
        {24,24000}
    };

    public GameObject objController;
    

    // skupna cena elementov
    public int skupaj = 0;
    // Use this for initialization
    void Start()
    {
        canvas.SetActive(false);
        
        
    }

    // Update is called once per frame
    void Update()
    {
        moneyState.text ="$" + miner.GetComponent<Xml>().money.ToString();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        objController.GetComponent<MessageManager>().buildingMessage(4);
        miner.GetComponent<Kopac>().premikanje = false;
        updateValues();
        canvas.SetActive(true);
        //gameObject.GetComponent<FuelTankUI>().showPanel();
        // prepove porabo goriv v stavbi
        miner.GetComponent<Xml>().useFuel = false;
    }

    private void OnTriggerExit2D(Collider2D collision) {
        skupaj = 0;
    }

    // aktivira jo gumb back in skrije panel
    public void hide() {
        canvas.SetActive(false);
        miner.GetComponent<Kopac>().premikanje = true;
        // prepove porabo goriv v stavbi
        miner.GetComponent<Xml>().useFuel = true;
    }

    // prebere vrednosti iz zalogovnika in izracuna zasluzek
    public void updateValues() {

        foreach (Transform child in mineralPanel.transform) {
            // dobi stevilko elementa
            int number = Int32.Parse(child.ToString().Split('_')[0]);
            
            // preveri ali je element panel
            if (child.tag == "panel") {
                // preveri ali stevilka obstaja v slovarju
                if (miner.GetComponent<Xml>().shramba.ContainsKey(number)) {
                    child.GetComponent<Image>().color = new Color32(219, 116, 116, 255);
                } else {
                    child.GetComponent<Image>().color = new Color32(127,174,197, 255);
                }
                
            }
            // sprehodim se po elementih od otroka in najdem text
            foreach (Transform element in child.transform) {
                // za dodajanje stevila kosov
                if (element.tag == "QTY") {
                    // preveri ali element obstaja v slovarju
                    if (miner.GetComponent<Xml>().shramba.ContainsKey(number)) {
                        element.GetComponent<Text>().text = miner.GetComponent<Xml>().shramba[number].ToString();
                    } else {
                        // ce vrednosti ni v slovarju ponastavi vrednost na 0
                        element.GetComponent<Text>().text = "0";
                    }
                }
                // izpise cene ki so shranjene v slovarju
                if (element.tag == "value") {
                    // preveri ali stevilka obstaja v slovarju
                    if (price.ContainsKey(number)) {
                        element.GetComponent<Text>().text = price[number].ToString() + "$";
                    }
                }
                // izracuna zasluzek za posamezen element ga izpise
                if (element.tag == "total") {
                    int total = 0;
                    // preveri ali element sploh obstaja v slovarju
                    if (miner.GetComponent<Xml>().shramba.ContainsKey(number)) {
                        total = price[number] * miner.GetComponent<Xml>().shramba[number];
                    }
                    // pristeje skupni ceni
                    skupaj += total;

                    element.GetComponent<Text>().text = "$" + total.ToString();
                }
            }
        }
        // izpise skupno ceno vseh elementov
        totalValueText.text = "Total $" + skupaj.ToString();
    }

    // ko pritisnem gumb proda elemente
    public void sellMinerals() {
        // preveri ali lahko predvaja zvok ali ne
        if (miner.GetComponent<Xml>().shramba.Count != 0) buySound.Play();

        miner.GetComponent<Xml>().money += skupaj;
        skupaj = 0;
        // pobrise slovar elementov
        miner.GetComponent<Xml>().shramba.Clear();
        // osvezi stanje
        updateValues();
    }
    
}
