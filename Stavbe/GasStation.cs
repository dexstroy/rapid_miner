﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GasStation : MonoBehaviour {
    public RectTransform fuelState;
    public RectTransform addedFuel;
    public Slider slider;

    public Text fuelStateText;
    public Text addedFuelText;
    public Text priceText;
    public Text buyButtonText;
    public Text moneyText;

    public GameObject panel;

    public AudioSource blagajnaSound;

    public GameObject adsObject;

    // kolicina goriva ki ga zelim kupiti
    float addFuel = 0;
    // true kadar imam zadosti denarja
    private bool canBuy = false;

    public GameObject miner;

    public GameObject objController;

    float procenti = 0;

    private float fuelPrice = 2;
    // Use this for initialization
    void Start () {
        // skrijem panel
        panel.SetActive(false);
        // dodam listener sliderju, ter povem katero metodo naj klice
        slider.onValueChanged.AddListener(delegate { ValueSliderChanged(); });
        //updateFuel();
	}
	
	
    private void OnTriggerEnter2D(Collider2D collision)
    {
        objController.GetComponent<MessageManager>().buildingMessage(3);
        adsObject.GetComponent<AdManager>().showBanner();
        // ustavi premikanje kopaca
        miner.GetComponent<Kopac>().premikanje = false;
        panel.SetActive(true);
        print("Enter GasStation");
        // prepove porabo goriv v stavbi
        miner.GetComponent<Xml>().useFuel = false;
        slider.value = 0;
        updateFuel();

    }


    public void ValueSliderChanged() {
        // da se izvede le na vsake 5%
        if((int)(slider.value * 100) % 5 == 0 ) updateFuel();
        addedFuel.anchorMax = new Vector2(procenti + slider.value * -(procenti - 1), 1);
    }

    // nastavi slider na trenutno kolicino goriva
    public void updateFuel() {
        // izracuna procente glede na tip tanka in kolicino goriva
        procenti = (miner.GetComponent<Xml>().fuelState) / miner.GetComponent<Xml>().fuelCapacity[miner.GetComponent<Xml>().selectedFuel];
        fuelState.anchorMax = new Vector2(procenti, 1);
        addedFuel.anchorMin = new Vector2(procenti, 0);

        addedFuel.anchorMax = new Vector2(procenti + slider.value * -(procenti - 1), 1);

        // spremeni text trenutnega goriva
        fuelStateText.text = Decimal.Round((Decimal)miner.GetComponent<Xml>().fuelState, 2).ToString() + "L";

        addFuel = slider.value * (miner.GetComponent<Xml>().fuelCapacity[miner.GetComponent<Xml>().selectedFuel] - miner.GetComponent<Xml>().fuelState);
        addFuel = (float)Decimal.Round((Decimal)addFuel, 2);
        addedFuelText.text = addFuel.ToString() + "L";

        priceText.text = (Math.Round(addFuel * fuelPrice)).ToString() + "$";

        // preverim ali je tank povn
        if ( miner.GetComponent<Xml>().fuelCapacity[miner.GetComponent<Xml>().selectedFuel] - miner.GetComponent<Xml>().fuelState <= 0.01) {
            canBuy = false;
            buyButtonText.text = "Full";
        }
        // preveri ali imam zadosti denarja
        else if (miner.GetComponent<Xml>().money >= addFuel * fuelPrice) {
            buyButtonText.text = "Buy";
            canBuy = true;
        } else {
            buyButtonText.text = "Not enough money!";
            canBuy = false;
        }
        // nastavim denar ki ga imam na voljo
        moneyText.text = "$" + miner.GetComponent<Xml>().money.ToString();
    }

    // ko pritisnem buyBotton
    public void buyButon() {
        // zahteva banner add
        
        if (canBuy) {
            if(addFuel != 0) blagajnaSound.Play();
            // vzamem denar
            miner.GetComponent<Xml>().money -= (int)Math.Round(addFuel * fuelPrice);
            // dodam gorivo
            miner.GetComponent<Xml>().fuelState += addFuel;
        }
        updateFuel();
        slider.value = 0;
    }

    // klice jo back button ko zelim zapustiti gasStation
    public void backButton() {
        // skrije ad
        adsObject.GetComponent<AdManager>().hideBanner();

        panel.SetActive(false);
        // spet nastavi da se lahko gorivo porablja
        miner.GetComponent<Xml>().useFuel = true;
        // dovoli premikanje kopaca
        miner.GetComponent<Kopac>().premikanje = true;
    }
}
