﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Repair : MonoBehaviour {

    public RectTransform healthState;
    public RectTransform addedHealth;
    public Slider slider;

    public Text healthStateText;
    public Text addedHealthText;
    public Text priceText;
    public Text buyButtonText;
    public Text moneyText;

    public GameObject panel;

    // za prikaz banerja
    public GameObject adsObject;

    // kolicina goriva ki ga zelim kupiti
    float addHealth= 0;
    // true kadar imam zadosti denarja
    private bool canBuy = false;

    public GameObject miner;

    private float heatlhPrice = 5;

    public AudioSource buySound;

    public GameObject objController;

    float procenti = 0;

    private void Start() {
        panel.SetActive(false);
        // dodam listener sliderju, ter povem katero metodo naj klice
        slider.onValueChanged.AddListener(delegate { ValueSliderChanged(); });
        //updateHealth();
    }

    private void OnTriggerEnter2D(Collider2D collision){
        objController.GetComponent<MessageManager>().buildingMessage(6);

        // prikaze reklamo
        adsObject.GetComponent<AdManager>().showBanner();

        // ustavi premikanje kopaca
        miner.GetComponent<Kopac>().premikanje = false;
        // prepove porabo goriv v stavbi
        miner.GetComponent<Xml>().useFuel = false;
        panel.SetActive(true);
        updateHealth();
    }

    public void ValueSliderChanged() {
        // da se izvede le na vsake 5%
        if ((int)(slider.value * 100) % 5 == 0) updateHealth();
        addedHealth.anchorMax = new Vector2(procenti + slider.value * -(procenti - 1), 1);
    }

    // nastavi slider na trenutno kolicino goriva
    public void updateHealth() {
        // izracuna procente glede na tip oklepa in kolicino zdravja
        procenti = (miner.GetComponent<Xml>().health) / miner.GetComponent<Xml>().hullHealth[miner.GetComponent<Xml>().selectedHull];
        healthState.anchorMax = new Vector2(procenti, 1);
        addedHealth.anchorMin = new Vector2(procenti, 0);

        addedHealth.anchorMax = new Vector2(procenti + slider.value * -(procenti - 1), 1);

        // spremeni text trenutnega zdravja
        healthStateText.text = Decimal.Round((Decimal)miner.GetComponent<Xml>().health, 2).ToString() + "H";

        addHealth = slider.value * (miner.GetComponent<Xml>().hullHealth[miner.GetComponent<Xml>().selectedHull] - miner.GetComponent<Xml>().health);
        addHealth = (float)Decimal.Round((Decimal)addHealth, 2);
        addedHealthText.text = addHealth.ToString() + "H";

        priceText.text = (Math.Round(addHealth * heatlhPrice)).ToString() + "$";

        // preverim ali ima polno zdravje
        if (miner.GetComponent<Xml>().hullHealth[miner.GetComponent<Xml>().selectedHull] - miner.GetComponent<Xml>().health <= 0.01) {
            canBuy = false;
            buyButtonText.text = "Full";
        }
        // preveri ali imam zadosti denarja
        else if (miner.GetComponent<Xml>().money >= addHealth * heatlhPrice) {
            buyButtonText.text = "Buy";
            canBuy = true;
        } else {
            buyButtonText.text = "Not enough money!";
            canBuy = false;
        }
        // nastavim denar ki ga imam na voljo
        moneyText.text = "$" + miner.GetComponent<Xml>().money.ToString();
    }

    // ko pritisnem buyBotton
    public void buyButon() {
        if (canBuy) {
            if(addHealth != 0)buySound.Play();
            // vzamem denar
            miner.GetComponent<Xml>().money -= (int)Math.Round(addHealth * heatlhPrice);
            // dodam zdravje
            miner.GetComponent<Xml>().health += addHealth;
        }
        updateHealth();
        slider.value = 0;
    }

    // klice jo back button ko zelim zapustiti repairShop
    public void backButton() {
        // skrije ad
        adsObject.GetComponent<AdManager>().hideBanner();

        panel.SetActive(false);
        // spet nastavi da se lahko gorivo porablja
        miner.GetComponent<Xml>().useFuel = true;
        // dovoli premikanje kopaca
        miner.GetComponent<Kopac>().premikanje = true;
        // prepove porabo goriv v stavbi
        miner.GetComponent<Xml>().useFuel = true;
    }
}
