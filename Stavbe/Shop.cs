﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Shop : MonoBehaviour {

    public RectTransform mainPanel;
    public RectTransform buttonPanel;

    public GameObject mainCanvas;

    // da lahko ustavi kopaca ko pride v store
    public Rigidbody2D miner;

    public AudioSource buySound;

    public GameObject objController;

    private void Start(){
        
        // skrijem canvas na zacetku igre
        mainCanvas.SetActive(false);

       
        
    }

    // aktivira ko kopac prileti  na trgovino
    private void OnTriggerEnter2D(Collider2D collision){
        // sporocilo ko igralec prvic obisce store
        objController.GetComponent<MessageManager>().buildingMessage(5);
        // ustavi kopaca
        miner.GetComponent<Kopac>().premikanje = false;
        // prepove porabo goriv v stavbi
        miner.GetComponent<Xml>().useFuel = false;
        mainCanvas.SetActive(true);
        // velikost glavnega panela da se lepo prilega gumbom, v tem panelu so potem vsi ostali
        //mainPanel.sizeDelta = new Vector2(-buttonPanel.rect.width, mainPanel.sizeDelta.y);
        mainPanel.sizeDelta = new Vector2(-Screen.height / 7, mainPanel.sizeDelta.y);
        // da ukaz da se aktivira panel za FuelTank in onemogoci vse ostale
        showPanel(3);
        transform.GetComponent<BombUI>().updateValues();

    }

    public void hideCanvas() {
        // omogoci premikanje kopaca
        miner.GetComponent<Kopac>().premikanje = true;
        // prepove porabo goriv v stavbi
        miner.GetComponent<Xml>().useFuel = true;
        mainCanvas.SetActive(false);
    }

    public void playBuySound() {
        buySound.Play();
    }

    // pokaze ustrezen panel
    public void showPanel(int i) {
        if (i == 1) transform.GetComponent<DrillUI>().showPanel(true);
        else transform.GetComponent<DrillUI>().showPanel(false);

        if (i == 2) transform.GetComponent<EngineUI>().showPanel(true);
        else transform.GetComponent<EngineUI>().showPanel(false);

        if (i == 3) transform.GetComponent<FuelTankUI>().showPanel(true);
        else transform.GetComponent<FuelTankUI>().showPanel(false);

        if (i == 4) transform.GetComponent<HullUI>().showPanel(true);
        else transform.GetComponent<HullUI>().showPanel(false);

        if (i == 5) transform.GetComponent<FanUI>().showPanel(true);
        else transform.GetComponent<FanUI>().showPanel(false);

        if (i == 6) transform.GetComponent<StorageUI>().showPanel(true);
        else transform.GetComponent<StorageUI>().showPanel(false);

        if (i == 7) transform.GetComponent<BombUI>().showPanel(true);
        else transform.GetComponent<BombUI>().showPanel(false);
    }
}
