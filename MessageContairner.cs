﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageContairner : MonoBehaviour {
    public Text t;
    public Animator anim;
	// Use this for initialization
	void Start () {
	}

    public void setText(string s) {
        t.text = s;
    }

    public void destroyAnimation() {
        anim.SetBool("unici", true);
    }

    public void delete() {
        GameObject.Find("Miner").GetComponent<Kopac>().pauseGame(false);
        Destroy(gameObject);
    }
}
