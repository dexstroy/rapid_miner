# Game RapidMiner

## About
Game developed in Unity where you have a miner that digs precious metals that are sold for money.
With money you can upgrade your miner to dig deeper and find more valuable metals.


![Local Image](images/mainmenu.jpg)

![Local Image](images/drilling.jpg)

![Local Image](images/electricbomb.jpg)

![Local Image](images/sell.jpg)

![Local Image](images/fueltank.jpg)

![Local Image](images/drill.jpg)

![Local Image](images/specials.jpg)