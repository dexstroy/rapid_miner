﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// na zaslon izpisuje stanje o kopacu
public class KopacStanje : MonoBehaviour {


    public Text visinaText;
    public Text moneyText;


    private void Update(){
        // prikaz visine na zaslon
        visinaText.text = Mathf.Round(10 * transform.position.y - 8).ToString();
        moneyText.text = "$" + transform.GetComponent<Xml>().money.ToString();
    }

    
}
