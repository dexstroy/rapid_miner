﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Kamera : MonoBehaviour {

    

    public GameObject kopac;

    private float height;
    private float width;

    // dolocitev do kje gre lahko kamera
    public float levoMeja;
    public float desnoMeja;

    // objekta ki imata colider, ki omejita premikanje kopaca
    public GameObject leftBorder;
    public GameObject rightBorder;

    public bool premikanje = true;

    // za gladko tranzicijo ko pridem iz povrsja
    private float meja;

    public GameObject ozadjeNarava;

    public SpriteRenderer zemljaOzadje;

    public byte barvaZemlje = 0;
    


	// Use this for initialization
	void Start () {
		Camera cam = Camera.main;
        height = 2f * cam.orthographicSize;
        width = height * cam.aspect;
 
        // preveri ali kamera gleda cez rob in jo popravi
        // leva stran
        if (transform.position.x - width / 2 < levoMeja) transform.position = new Vector3(levoMeja + width / 2, kopac.transform.position.y, -15);
        // desna stran
        if (transform.position.x + width / 2 > desnoMeja) transform.position = new Vector3(desnoMeja - width / 2, kopac.transform.position.x, -15);

        // nastavi borderja levo in desno
        leftBorder.transform.position = new Vector2(0.5f, 0);
        rightBorder.transform.position = new Vector2(desnoMeja, 0);

        meja = (height / 2) - 2;

    }
	
	// Update is called once per frame
	void Update () {

        //ozadjeNarava.transform.localPosition = new Vector2((transform.position.x - 15.5f) * -(( (ozadjeNarava.GetComponent<SpriteRenderer>().size.x - width) / 2 ) / ( (32 - width)/2 )) , ozadjeNarava.transform.localPosition.y);

        if (kopac.transform.position.y < -999 || !premikanje && transform.position.y < -999) {
            transform.position = new Vector3(15.5f, -1005, -15);
            // prekine trenutni update ker ostala koda ni nujna
            transform.GetComponent<Camera>().orthographicSize = 16/Camera.main.aspect;
            return;
        }

        transform.GetComponent<Camera>().orthographicSize = 4;


        // premakne ozdadje da zgleda dinamicno, ko ni na povrsju se koda preneha klicati
        if(transform.position.y > - 15) ozadjeNarava.transform.localPosition = new Vector2((transform.position.x - 15.5f) * -(((ozadjeNarava.GetComponent<SpriteRenderer>().size.x - width) / 2) / ((32 - width) / 2)), 
                                                            (transform.position.y -3) * -(((ozadjeNarava.GetComponent<SpriteRenderer>().size.y - height) / 2) / (( 40) / 2)));
        //ozadjeNarava.transform.localPosition = new Vector2(0, 0);
        if (premikanje) {
            // preveri ali je kopac v varnem obmocju
            if (kopac.transform.position.x - width / 2 > levoMeja && kopac.transform.position.x + width / 2 < desnoMeja) {
                // kamero da na isto pozicijo kot kopaca

                // preveri ali je na poovrsju in dvigne pogled kamere
                //&& kopac.transform.position.y > -1000)
                if (kopac.transform.position.y < 0 ) { 
                    // znizuje mejo dokler ni 0
                    if (meja > 0) {
                        meja -= 2f * Time.deltaTime;
                    }
                }
                else if (meja < (height / 2) - 2 ) {
                    meja += 2f * Time.deltaTime;
                }
                
                    
                
                
                    transform.position = new Vector3(kopac.transform.position.x, kopac.transform.position.y + meja, -15);
                
                
            }
            // ce je na levi strani kamero poravna ob levo stran
            else if (kopac.transform.position.x - width / 2 < levoMeja) {
                transform.position = new Vector3(levoMeja + width / 2, kopac.transform.position.y + meja, -15);
            } 
            // ce je na desni strani kamero poravna ob desno stran
            else if (kopac.transform.position.x + width / 2 > desnoMeja) {
                transform.position = new Vector3(desnoMeja - width / 2, kopac.transform.position.y + meja, -15);
            } 
        }

        // izracuna barvo za ozadje pod zemljo
        barvaZemlje = (byte)(0.1962 * transform.position.y + 254.803);
        zemljaOzadje.color = new Color32(255, barvaZemlje, barvaZemlje, 255);
    }
}
