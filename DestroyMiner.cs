﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMiner : MonoBehaviour {

    public Rigidbody2D[] elementi;

    public float explosionSpeed = 3;
	// Use this for initialization
	void Start () {
        foreach (Rigidbody2D element in elementi) {
            // doda random force elementu
            element.velocity = getVelocity();
            // doda random velocity elementu
            element.angularVelocity = Random.Range(-explosionSpeed * 100, explosionSpeed * 100);
        }
            
        InvokeRepeating("destroyItself", 2, 1);
    }

    // vrne smer izstrelka 
    public Vector2 getVelocity() {
        return new Vector2( Random.Range(-explosionSpeed, explosionSpeed), Random.Range(-explosionSpeed, explosionSpeed));
    }

    // da ukaz minerju da se povono prikaze, potem pa izbrise animacijo
    public void destroyItself() {
        // najde vse objekte z tagom miner
        GameObject[] objekti = GameObject.FindGameObjectsWithTag("miner");
        foreach (GameObject miner in objekti) {
            miner.GetComponent<Kopac>().spawnMiner();
        }

        Destroy(gameObject);
    }
}
