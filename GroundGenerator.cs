﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 Ground generator generira tla (postavlja kocke zemlje in rud).
 Ko se skripta prvic pozene nakljucno generira dvodimenzionalno tabelo rudnin in jo nato izrise
 */
public class GroundGenerator : MonoBehaviour {
    // lista rudnin, za razmnozevanje
    public GameObject[] list;


    // dvodimenzionalna tabela rudnin
    int[,] elementi;
    System.Random random1 = new System.Random();

    // globina do katere je ze generiral elemente
    private int generirana_globina = 20;

    private int top = -1;
    private int bottom = -20;




    void Start() {
        // tabela velikosti, 32 vrstic, ter 1000 stolpcev
        elementi = new int[32, 1000];

        /*
         Elementi na planetu Zemlja
         
         ____element__________verjetnost__sprememba_verjetnosti_na_60m
         |0 - air        | 10           | 0                             |
         |1 - earth      | 500          | 0                             |
         |2 - coal       | 20           | 0                             |
         |3 - iron       | 10           | 0                             |
         |4 - copper     | 5            | +5                            |
         |5 - aluminium  | 0            | +5                            |
         |6 - gold       | -5           | +5                            
         |7 - platinum   | -8           | +4
         |8 - titan      | -12          | +4
         |9 - Amethyst   | -15          | +3
         |10 - sapphire  | -21          | +3
         |11 - emerald   | -18          | +2
         |12 - ruby      | -22          | +2 
         |13 - diamond   | -13          | +1
         |14 - stone     | -40          | +20

         Redke najdbe na planetu zemlja
         ______element______verjetnost___sprememba_verjetnosti_na_60m
         |15 - amber     | 0           | +1
         |16 - skeleton  | 0           | +1
         |17 - mine      | 0           | +1
         |18 - chest     | 0           | +1
         |19 - fossil    | -5          | +1
         |20 - statute   | -5          | +1
         |21 - ring      | -8          | +1
         |22 - vase      | -8          | +1
         |23 - coin      | -12         | +1
         |24 - scroll    | -12         | +1
         ...
         */

        // polnitev dvodimenzionalne tabele s nakljucnimi elementi
        for (int vrstica = 0; vrstica < 1000; vrstica++) {
            for (int stolpec = 0; stolpec < 32; stolpec++) {
                elementi[stolpec, vrstica] = generirajElement(vrstica);
                //Debug.Log(elementi[stolpec, vrstica]);
            }
        }
        izris(1, generirana_globina);
        
        // ukaz se izvede v bazi ko se vsi podatki nalozijo
        gameObject.GetComponent<Xml>().uniciIzkopane();

    }

    private void Update() {

        // ko pride do bosa se izris elemetov prekine, ker pod tem ni rude
        if (transform.position.y >= -990.5f) {
            // preveri ali je dovolj na globini za izris elementov
            if ((transform.position.y + 9.5f) < -0.6f) {
                // izbris zgoraj
                if ((transform.position.y + 9.5f) < (float)top) {
                    deleteLine(top);
                    top -= 1;
                }
                // izris zgoraj
                if ((transform.position.y + 9.5f) > (float)(top + 1)) {
                    top += 1;
                    izris(top, top);
                    transform.GetComponent<Xml>().uniciIzkopaneVrstica(top);
                }

                // izbris spodaj
                if ((transform.position.y - 9.5f) > (float)bottom) {
                    deleteLine(bottom);
                    bottom += 1;
                }
                // izris spodaj
                if ((transform.position.y - 9.5f) < (float)(bottom - 1)) {
                    bottom -= 1;
                    izris(bottom, bottom);
                    transform.GetComponent<Xml>().uniciIzkopaneVrstica(bottom);
                }
            }
            //if (Input.GetKeyDown(KeyCode.Space)) izris(30, 30);
        }
        
    }

    // funkcija ki prejme globino in generira glede na globino nakljucni element ki je stevilo
    public int generirajElement(int globina) {

        int izracun = 0;

        int vrednost = globina / 60;
        int air = 10 + (vrednost * 0); if (air < 0) air = 0;
        int earth = 300 + (vrednost * 0); if (earth < 0) earth = 0;
        int coal = 20 + (vrednost * 0); if (coal < 0) coal = 0;
        int iron = 10 + (vrednost * 0); if (iron < 0) iron = 0;
        int copper = 5 + (vrednost * 1); if (copper < 0) copper = 0; if (copper > 20) copper = 20;
        int aluminium = 0 + (vrednost * 5); if (aluminium < 0) aluminium = 0; if (aluminium > 15) aluminium = 15;
        int gold = -5 + (vrednost * 5); if (gold < 0) gold = 0; if (gold > 15) gold = 15;
        int platinum = -8 + (vrednost * 4); if (platinum < 0) platinum = 0; if (platinum > 12) platinum = 15;
        int titan = -12 + (vrednost * 4); if (titan < 0) titan = 0; if (titan > 12) titan = 12;
        int Amethyst = -15 + (vrednost * 3); if (Amethyst < 0) Amethyst = 0; if (Amethyst > 12) Amethyst = 12;
        int sapphire = -21 + (vrednost * 3); if (sapphire < 0) sapphire = 0; if (sapphire > 12) sapphire = 12;
        int emerald = -18 + (vrednost * 2); if (emerald < 0) emerald = 0; if (emerald > 10) emerald = 10;
        int ruby = -22 + (vrednost * 2); if (ruby < 0) ruby = 0; if (ruby > 8) ruby = 8;
        int diamond = -13 + (vrednost * 1); if (diamond < 0) diamond = 0; if (diamond > 5) diamond = 5;
        int stone = -60 + (vrednost * 20); if (stone < 0) stone = 0; if (stone > 400) stone = 400;

        int amber = 0 + (vrednost * 1); if (amber < 0) amber = 0; if (amber > 1) amber = 1;
        int skeleton = 0 + (vrednost * 1); if (skeleton < 0) skeleton = 0; if (skeleton > 1) skeleton = 1;
        int mine = 0 + (vrednost * 1); if (mine < 0) mine = 0; if (mine > 1) mine = 1;
        int chest = 0 + (vrednost * 1); if (chest < 0) chest = 0; if (chest > 1) chest = 1;
        int fossil = -5 + (vrednost * 1); if (fossil < 0) fossil = 0; if (fossil > 1) fossil = 1;
        int statute = -5 + (vrednost * 1); if (statute < 0) statute = 0; if (statute > 1) statute = 1;
        int ring = -8 + (vrednost * 1); if (ring < 0) ring = 0; if (ring > 1) ring = 1;
        int vase = -8 + (vrednost * 1); if (vase < 0) vase = 0; if (vase > 1) vase = 1;
        int coin = -12 + (vrednost * 1); if (coin < 0) coin = 0; if (coin > 1) coin = 1;
        int scroll = -12 + (vrednost * 1); if (scroll < 0) scroll = 0; if (scroll > 1) scroll = 1;

        int vsota = air + earth + stone + coal + iron + copper + aluminium + gold + platinum + titan + Amethyst + sapphire + emerald + ruby + diamond +
            amber + skeleton + mine + chest + fossil + statute + ring + vase + coin + scroll;

        int randomst = random1.Next(1, (vsota + 1));

        izracun += air; if (randomst <= izracun) return 0;
        izracun += earth; if (randomst <= izracun) return 1;
        izracun += coal; if (randomst <= izracun) return 2;
        izracun += iron; if (randomst <= izracun) return 3;
        izracun += copper; if (randomst <= izracun) return 4;
        izracun += aluminium; if (randomst <= izracun) return 5;
        izracun += gold; if (randomst <= izracun) return 6;
        izracun += platinum; if (randomst <= izracun) return 7;
        izracun += titan; if (randomst <= izracun) return 8;
        izracun += Amethyst; if (randomst <= izracun) return 9;
        izracun += sapphire; if (randomst <= izracun) return 10;
        izracun += emerald; if (randomst <= izracun) return 11;
        izracun += ruby; if (randomst <= izracun) return 12;
        izracun += diamond; if (randomst <= izracun) return 13;
        izracun += stone; if (randomst <= izracun) return 14;


        izracun += amber; if (randomst <= izracun) return 15;
        izracun += skeleton; if (randomst <= izracun) return 16;
        izracun += mine; if (randomst <= izracun) return 17;
        izracun += chest; if (randomst <= izracun) return 18;
        izracun += fossil; if (randomst <= izracun) return 19;
        izracun += statute; if (randomst <= izracun) return 20;
        izracun += ring; if (randomst <= izracun) return 21;
        izracun += vase; if (randomst <= izracun) return 22;
        izracun += coin; if (randomst <= izracun) return 23;
        izracun += scroll; if (randomst <= izracun) return 24;

        return 999;
    }

    // izrise elemente na zaslon od, do
    public void izris(int from, int to) {
        // obrne jih v pozitivno smer ce so negativni
        if (from < 0) from = -from;
        if (to < 0) to = -to;
        // stolpci
        for (int i = from; i <= to; i++) {
            // vrstice
            for (int j = 0; j < 32; j++) {
                Instantiate(list[elementi[j, i]], new Vector3(j, -i, 0), Quaternion.identity);

            }
        }

    }

    // izbrise doloceno vrstico
    public void deleteLine(int vrstica) {
        // gre skozi vse kocke v vrstici
        for (int i = 0; i < 32; i++) {
            // dobi vse objekte v obmocju vrstice
            foreach (Collider2D col in Physics2D.OverlapBoxAll(new Vector2(15.5f, vrstica), new Vector2(31, 0.5f), 0f)) {
                Destroy(col.gameObject);

            }

        }
    }

    // unici vse elemente v generiranem odseku in spawna od zgoraj elemente
    public void resetGround() {
        // unici vse elemente kjer je miner unicen
        for (int b = bottom; b <= top; b++) {
            deleteLine(b);
        }
        // narise od zgoraj ground
        izris(1, generirana_globina);

        // nastavi top in bottom 
        top = -1;
        bottom = -20;
        // ker je miner na vrhu lahko unicim vse izkopane
        gameObject.GetComponent<Xml>().uniciIzkopane();
    }

}
